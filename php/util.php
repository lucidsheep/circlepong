<?php

function encrypt($input, $sk)
{
	return openssl_encrypt($input, 'aes-256-ecb', $sk);
}

function decrypt($input, $sk)
{
	return openssl_decrypt($input, 'aes-256-ecb', $sk);
}

function echoError($errorCode, $errorDesc, $sk)
{
	$ret["status_code"] = $errorCode;
	$ret["status_detail"] = $errorDesc;
	echo encrypt(json_encode($ret), $sk);
}
?>
