<?php
require 'vars.php';
require 'util.php';

$cabID = htmlspecialchars($_GET["id"]);
$cabPW = htmlspecialchars($_GET["pw"]);
$input = htmlspecialchars($_GET["input"]);
$func = htmlspecialchars($_GET["func"]);
// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);

// Check connection
if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
}
//echo "Connected successfully";
$sql = "SELECT cab_id, password, secret_key from cabs where cab_id='" . $cabID . "'";
//$sql = "SELECT cab_id, password, secret_key FROM cabs";
//echo "<br>" . $sql;

$result = $conn->query($sql);

if ($result->num_rows > 0) {
  // output data of each row
  $row = $result->fetch_assoc();
  $sk = $row["secret_key"]; 
  $cid = $row["cab_id"];

  if($func == "encrypt")
	  echo encrypt($input, $sk);
  if($func == "decrypt")
	  echo decrypt($input, $sk);
	if($func == "startgame")
	{
		//input = list of player ids already in game
		$playerData = json_decode(decrypt($input, $sk), true);
		if($playerData == NULL) {
			$playerData["players"][0] = 0;
			$playerData["players"][1] = 0;
			$playerData["set_id"] = 0;
		}
		
		//check if there's a game in progress already
		$sql = "select game_id, cab_id, game_complete from games where game_complete=0 and cab_id='" . $cid . "'";
		$result = $conn->query($sql);
		if($result->num_rows > 0)
		{
			$ret = $result->fetch_assoc();
			$ret["status_code"] = 1;
			$gameID = $ret["game_id"];
			echo encrypt(json_encode($ret), $sk);
			//update start time to now
			$sql = "update games set start_date=now() where game_id=" . $gameID;
			$conn->query($sql);
			//update with any new player data
			for($i = 0; $i < 2; $i++)
			{
				$player = $playerData["players"][$i];
				if($player == NULL) $player = 0;
				$sql = "update game_players set player_id=" . $player . " where game_id=" . $gameID . " and side_id=" . $i;
				$conn->query($sql);
			}
		} else 
		{
			$setID = $playerData["set_id"];
			if($setID == 0)
			{
				//client isn't in a set, so we need to make a new one
				$sql = "insert into sets (player_one_id, player_two_id) values (" . $playerData["players"][0] . ", " . $playerData["players"][1] . ")";
				if($conn->query($sql) === FALSE)
				{
					echoError(10, "SQL error creating set", $sk);
					return;
				}
				$setID = $conn->insert_id;
			}
			$ret["set_id"] = $setID;
			$sql = "insert into games (cab_id, set_id, start_date, game_complete) values ('" . $cid . "', '" . $setID . "', '" . date('Y-m-d H:i:s') . "', '0')";
			if ($conn->query($sql) === TRUE)
			{
				$ret["game_id"] = $conn->insert_id;
				echo encrypt(json_encode($ret), $sk);
				//add game_player entries
				for($i = 0; $i < 2; $i++)
				{
					$player = $playerData["players"][$i];
					if($player == NULL) $player = 0;
					$sql = "insert into game_players (game_id, side_id, player_id) values (" . $ret["game_id"] . ", " . $i . ", " . $player . ")";
					$conn->query($sql);
				}
			}
			else
			{
				echoError(11, "SQL error creating game", $sk);
			}
		}
	}
if($func == "checkupdates")
{
	$sql = "select * from updates where cab_id='" . $cid . "'";
	$result = $conn->query($sql);
	$rownum = 0;
	while($result->num_rows == 0)
	{
		sleep(1);
		$result = $conn->query($sql);
	}
	if($result->num_rows > 0)
	{
		while($row = $result->fetch_assoc()) {
			$sql = "select * from players where player_id = '" . $row["player_id"] . "'";
			$userResult = $conn->query($sql);
			
			if ($userResult->num_rows > 0) {
				$ret["players"][$rownum] = $userResult->fetch_assoc();
				$ret["players"][$rownum]["side_id"] = $row["update_code"];
				$rownum++;
			}
		}
		echo encrypt(json_encode($ret), $sk);
	}
	//now delete all updates from server for tihs cab
	$sql = "delete from updates where cab_id='" . $cabID . "'";
	$conn->query($sql);
}
if($func == "endgame")
{
	$decrypted = decrypt($input, $sk);
	if($decrypted === FALSE)
	{
		echoError(2, "decryption failed", $sk);
		return;	
	}
	$json = json_decode($decrypted, true);
	if($json == NULL ){
		echoError(3,"json is malformed", $sk);
		return;
	}
	$gameID = $json["game_id"];
	if($gameID == NULL ){
		echoError(4,"game ID not found", $sk);
		return;
	}
	$sql = "select game_id, cab_id, game_complete from games where game_id=" . $json["game_id"] . " and cab_id=" . $cabID . " and game_complete=0";
	$result = $conn->query($sql);
	if($result === FALSE)
	{
		echoError(5, "SQL error on finding game to complete", $sk);
		return;
	}
	if($result->num_rows > 0)
	{
		$sql = "update games set game_complete=1"
		. ", average_rally=" . $json["average_rally"] 
		. ", longest_rally=" . $json["longest_rally"]
		. ", game_time=" . $json["game_time"]
		. " where game_id=" . $json["game_id"];
		
		$conn->query($sql);
		//get game stats
		for($i = 0; $i < 2; $i++)
		{
			$playerStats = $json["game_players"][$i];
			if($playerStats != NULL)
			{
				$sql = "update game_players set points=" . $playerStats["points"]
				. ", player_id=" . $playerStats["player_id"]
				. ", result_type=" . $playerStats["result_type"]
				. ", shots_slice=" . $playerStats["shots_slice"]
				. ", shots_power=" . $playerStats["shots_power"]
				. ", shots_smash=" . $playerStats["shots_smash"]
				. ", shots_ace=" . $playerStats["shots_ace"]
				. ", shots_fault=" . $playerStats["shots_fault"]
				. " where game_id=" . $json["game_id"] . " and side_id=" . $i;
				$conn->query($sql);
			}
		}
		$ret["status_code"] = 0;
		echo encrypt(json_encode($ret), $sk);
	} else
	{
		$ret["status_code"] = 3;
		$ret["status_detail"] = "game ID not found or is already marked as complete";
		echo encrypt(json_encode($ret), $sk);
	}
}
if($func == "error")
{
	echoError(69, "error test successful", $sk);
}
if($func == "config")
{
	$sql = "select * from cab_config where cab_id='" . $cid . "'";
	$result = $conn->query($sql);
	if($result->num_rows > 0)
	{
		//todo - manage multiple rows, pick the most recent? flag for current config?
		$row = $result->fetch_assoc();
		echo encrypt(json_encode($row), $sk);
	} else {
		echoError(70, "no config found for this cab ID", $sk);
	}

}
}else echo "userID not found";

$conn->close();
?>
 