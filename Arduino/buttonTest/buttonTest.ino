#include <ezButton.h>
#include "FastLED.h"
#include <SoftPWM.h>

#define BUTTON_PIN 46
#define BUTTON_LED 48
#define BUTTON2_PIN 42
#define BUTTON2_LED 44


ezButton button(BUTTON_PIN);  // create ezButton object that attaches to pin 7;
ezButton button2(BUTTON2_PIN);  // create ezButton object that attaches to pin 7;


unsigned long msLast = 0;
unsigned long msCur = 0;
int nextUpdate = 0;
bool isDown = false;

 int tapCounts[] = {0,0,0,0,0,0,0,0,0,0};
 int tapIndex = 0;

void setup() {
  // serial plotter for testing
  Serial.begin(9600);
  SoftPWMBegin();
  button.setDebounceTime(10); // set debounce time to 10 milliseconds
  button.setCountMode(COUNT_FALLING); // count when button state goes from HIGH to LOW
    button2.setDebounceTime(10); // set debounce time to 10 milliseconds
  button2.setCountMode(COUNT_FALLING); // count when button state goes from HIGH to LOW
  SoftPWMSet(BUTTON_LED,0);
  SoftPWMSet(BUTTON2_LED,0);
}

void loop() {

  button.loop(); // MUST call the loop() function first
  button2.loop();
  msCur = millis();
  nextUpdate -= msCur - msLast;
  msLast = msCur;

 if(button.isPressed())
 {
   isDown = true;
   SoftPWMSetPercent(BUTTON_LED, 100);   
 }
 else if(button.isReleased())
 {
   isDown = false;
  SoftPWMSetPercent(BUTTON_LED, 0);   
 }
 if(button2.isPressed())
 {
   isDown = true;
   SoftPWMSetPercent(BUTTON2_LED, 100);   
 }
 else if(button2.isReleased())
 {
   isDown = false;
  SoftPWMSetPercent(BUTTON2_LED, 0);   
 }
 if(nextUpdate <= 0)
 {
   nextUpdate += 100;
   tapCounts[tapIndex] = button.getCount() + button2.getCount();
   button.resetCount();
   button2.resetCount();
   int sum = 0;
    for(int i = 0; i < 10; i++)
    {
        sum += tapCounts[i];
    }
    Serial.println("TPS: " + String(sum));
    tapIndex = tapIndex >= 9 ? 0 : tapIndex + 1;
 } 
}

/*
#include <ezButton.h>

#define CLK 2
#define DIO 3

ezButton button(7);  // create ezButton object that attaches to pin 7;

unsigned long timer = 0;

float tps_hover = 3.97;
//float tps_ = 
float tps_max_rise = 6.0;
float tps_max_rise_speed = 4.0;

unsigned long msLast = 0;
unsigned long msCur = 0;
int nextUpdate = 100;

 int tapCounts[] = {0,0,0,0,0,0,0,0,0,0};
 int tapIndex = 0;

 bool stickInProgress = false;
 unsigned long lastButtonPress = 0;

void setup() {
  // serial plotter for testing
  Serial.begin(9600);
  
  button.setDebounceTime(10); // set debounce time to 50 milliseconds
  button.setCountMode(COUNT_FALLING); // count when button state goes from HIGH to LOW
  
}

void loop() {

  button.loop(); // MUST call the loop() function first

  msCur = millis();
  nextUpdate -= msCur - msLast;
  msLast = msCur;

  if(stickInProgress && msCur - lastButtonPress >= 250)
  {
    stickInProgress = false;
    for(int i = 0; i < 10; i++)
      tapCounts[i] = 0;
    Serial.println("Scrape ended");

  }
  if(button.isPressed())
  {
    lastButtonPress = msCur;
  }
 if(nextUpdate <= 0)
 {
   nextUpdate += 100;
   tapCounts[tapIndex] = button.getCount();
   button.resetCount();
   int sum = 0;
    for(int i = 0; i < 10; i++)
    {
        sum += tapCounts[i];
    }
    if(!stickInProgress && sum >= 4)
    {
      stickInProgress = true;
      Serial.println("Scrape started");
    } else if(stickInProgress)
    {
      int diff = sum - 4;
      Serial.println("Scrape held +" + String(diff));
    } 
    tapIndex = tapIndex >= 9 ? 0 : tapIndex + 1;
 } 
  


}
*/