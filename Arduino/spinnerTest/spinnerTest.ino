 #define outputA 14
 #define outputB 15

 int counter = 0; 
 int revolutions = 0;
 int PPR = 600;
 int aState;
 int aLastState;  
 unsigned long lastTime = 0;
 unsigned long curTime = 0;
 long updateMSCount = 10000;
 int pulseCounts[] = {0,0,0,0,0,0,0,0,0,0};
 int pulseIndex = 0;
 void setup() { 
   pinMode (outputA,INPUT);
   pinMode (outputB,INPUT);
   
   Serial.begin (9600);
   // Reads the initial state of the outputA
   aLastState = digitalRead(outputA);   
 } 

 void loop() { 
   aState = digitalRead(outputA); // Reads the "current" state of the outputA
   // If the previous and the current state of the outputA are different, that means a Pulse has occured
   if (aState != aLastState){     
     // If the outputB state is different to the outputA state, that means the encoder is rotating clockwise
     pulseCounts[pulseIndex]++;
     if (digitalRead(outputB) != aState) { 
       counter ++;
       if(counter >= PPR)
       {
         counter -= PPR;
         revolutions++;
       }
     } else {
       counter --;
       if(counter <= -PPR)
       {
          counter += PPR;
          revolutions--;
       }
     }
   } 
   aLastState = aState; // Updates the previous state of the outputA with the current state
   curTime = micros() + 4; //4 micros for function execution
  int timeIncrement = curTime < lastTime ? 0 : curTime - lastTime; //account for overflow
  updateMSCount -= timeIncrement;
  lastTime = curTime;
  if(updateMSCount <= 0)
  {
    updateMSCount += 10000;
    //Serial.print("PPS: ");
    //Serial.println(sum, DEC);
    //Serial.print("Revs ");
    //Serial.print(revolutions, DEC);
    //Serial.print(" PC ");
    //Serial.println(counter, DEC);
    pulseIndex = pulseIndex >= 9 ? 0 : pulseIndex + 1;
    if(pulseIndex == 0)
    {
      int sum = 0;
      for(int i = 0; i < 10; i++)
      {
          sum += pulseCounts[i];
      }
      float avg = (float)sum / 60.0;
      Serial.print("RPS ");
      Serial.println(avg, DEC);
    }
    pulseCounts[pulseIndex] = 0;
  }
 }