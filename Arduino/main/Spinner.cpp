#include "Spinner.h"
#include "Arduino.h"

   Spinner::Spinner(int a, int b)
   {
      pinA = a;
      pinB = b;
      pinMode (pinA,INPUT);
      pinMode (pinB,INPUT);

      lastState = digitalRead(pinA); 
   }
   void Spinner::loop()
   {
    int aState = digitalRead(pinA);   
      if (aState != lastState){     
        if (digitalRead(pinB) != aState) { 
          delta++;
        } else {
          delta--;
        }
      } 
      lastState = aState;
   }
   int Spinner::GetDeltaAndReset()
   {
     int ret = delta;
     delta = 0;
     return ret;
   }
