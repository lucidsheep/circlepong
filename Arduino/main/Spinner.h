#ifndef Spinner_h
#define Spinner_h


class Spinner
{
  private:
    int pinA;
    int pinB;
    int delta;
    int lastState;
  public:
   Spinner(int a, int b);
   void loop();
   int GetDeltaAndReset();

};

#endif