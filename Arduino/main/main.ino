#include "FastLED.h"
#include <SoftPWM.h>
#include <EEPROM.h>
#include "LEDButton.h"
#include "Spinner.h"

#define DEBUG_LOG false

#define NUM_LEDS 74
#define LED_PIN 8
#define LED_STRIP_ENABLED false

#define LED_P1LEFT 48
#define BTN_P1LEFT 46
#define LED_P1RIGHT 44
#define BTN_P1RIGHT 42

#define LED_P2LEFT 49
#define BTN_P2LEFT 47
#define LED_P2RIGHT 45
#define BTN_P2RIGHT 43

#define SPIN_P1_1 22
#define SPIN_P1_2 24

#define SPIN_P2_1 23
#define SPIN_P2_2 25

const CRGB bluePaddleColor = CRGB::Blue;
const CRGB redPaddleColor = CRGB::Red;
const CRGB collidePaddleColor = CRGB::DarkViolet;
const CRGB neutralColor = CRGB::Black; //CRGB::DarkViolet;

const CRGB blueAnimColor = CRGB::Cyan;
const CRGB redAnimColor = CRGB::OrangeRed;

CRGB leds[NUM_LEDS];

LEDButton P1LeftButton(LED_P1LEFT, BTN_P1LEFT);
LEDButton P1RightButton(LED_P1RIGHT, BTN_P1RIGHT);
LEDButton P2LeftButton(LED_P2LEFT, BTN_P2LEFT);
LEDButton P2RightButton(LED_P2RIGHT, BTN_P2RIGHT);
LEDButton* buttons[4] = {&P1LeftButton, &P1RightButton, &P2LeftButton, &P2RightButton };

Spinner P1Spinner(SPIN_P1_1, SPIN_P1_2);
Spinner P2Spinner(SPIN_P2_1, SPIN_P2_2);
Spinner* spinners[2] = {&P1Spinner, &P2Spinner };

int redCenter = -1;
int blueCenter = -1;

bool shouldShowPaddles = false;

int animCode = -1;
int animFrame = 0;
int animOriginPoint = 0;
int animCurrentPoint = 0;
int animTick = 0;
int animSpecial = 0;
CRGB animColor = CRGB::Black;

int bluePaddleDeltas[4] = {0,0,0,0};
int redPaddleDeltas[4] = {0,0,0,0};
int blueSparkPosition = 0;
int blueSparkSpeed = 0;
int blueSparkTick = -1;
int blueSparkLastCenter = -1;
int redSparkPosition = 0;
int redSparkSpeed = 0;
int redSparkTick = -1;
int redSparkLastCenter = -1;

int sparkUpdateTick = 0;

int brightness = 255;
int buttonBrightness = 100;
bool shouldUpdateDisplay = false;

unsigned long lastTime = 0;
unsigned long curTime = 0;
int nextSerialCountdown = 0;
unsigned int animFrameMS = 1000 / 120;

void setup() {
  //load values from memory
  initMemory();
  //setup strip
  if(LED_STRIP_ENABLED)
  {
    FastLED.addLeds<NEOPIXEL, LED_PIN>(leds, NUM_LEDS); 
    for(int i = 0; i < NUM_LEDS; i++){
      leds[i] = CRGB::Purple;
    }
  }
  setupColors();
  pinMode(LED_BUILTIN, OUTPUT);
  Serial.begin(19200);
  //setup buttons
  SoftPWMBegin();
  //P1LeftButton.setup();
  //P1RightButton.setup();
  for(int i = 0; i < 4; i++)
  {
    buttons[i]->setup();
  }

}
void initMemory()
{
  int memVersion = EEPROM.read(0);
  bool updated = false;
  if(memVersion == 255 || memVersion == 0) //first init may have 255
  {
    memVersion = 1;
    EEPROM.write(1, 255); //brightness
    EEPROM.write(2, 100); //buttonbrightness
    updated = true;
  }
    
  memVersion = 1; //latest version
  
  if(updated)
    EEPROM.write(0, memVersion);

  //load variable data
  brightness = EEPROM.read(1);
  buttonBrightness = EEPROM.read(2);
}
void setupColors()
{
  if(LED_STRIP_ENABLED)
    FastLED.setBrightness(brightness);
}
int checkBound(int in)
{
  if(in < 0) return NUM_LEDS + in;
  if(in >= NUM_LEDS) return in - NUM_LEDS;
  return in;
}

CRGB reduceBrightness(CRGB orig, uint8_t level)
{
  orig %= level;
  return orig;
}

void setColors()
{
  for(int i = 0; i < NUM_LEDS; i++){
   leds[i] = neutralColor; 
  }
  if(shouldShowPaddles)
    displayPaddles();
  if(animCode > -1)
    displayAnim();
  displaySparks();
}

void displaySparks()
{
  if(blueSparkTick >= 0)
    leds[blueSparkPosition] = blueAnimColor;
  if(redSparkTick >= 0)
    leds[redSparkPosition] = redAnimColor;
}
void displayPaddles()
{
  if(blueCenter > -1)
    setPaddlePixel(blueCenter, bluePaddleColor, collidePaddleColor, 0, 2);
  if(redCenter > -1)
    setPaddlePixel(redCenter, redPaddleColor, collidePaddleColor, 0, 2);
}

void setPaddlePixel(int pos, CRGB intendedColor, CRGB overlapColor, int dir, int spread)
{
  if(leds[pos] == neutralColor)
    leds[pos] = intendedColor;
   else
    leds[pos] = overlapColor;
  if(spread > 0)
  {
    if(dir <= 0)
      setPaddlePixel(checkBound(pos - 1), intendedColor, overlapColor, -1, spread - 1);
    if(dir >= 0)
      setPaddlePixel(checkBound(pos + 1), intendedColor, overlapColor, 1, spread - 1);
  }
}

void displayAnim()
{
  switch(animCode)
  {
    case 200: case 201:
      if(animFrame == 0)
        leds[animOriginPoint] = animColor;
      else
      {
        leds[checkBound(animOriginPoint + animFrame)] = animColor;
        leds[checkBound(animOriginPoint - animFrame)] = animColor;
      }
      break;
    case 202: case 203: case 204: case 205:
      if(animCode > 203 && animFrame < 2)
      {
        //smash paints entire strip white at the start
        for(int i = 0; i < NUM_LEDS; i++)
          leds[i] = CRGB::White;
        break;
      }
      //p-wave
      if(animFrame == 0)
        leds[animOriginPoint] = animColor;
      else
      {
        leds[checkBound(animOriginPoint + animFrame)] = animColor;
        leds[checkBound(animOriginPoint - animFrame)] = animColor;
      }
      //s-wave
      if(animFrame > 3)
      {
        int aPos = (animFrame) / 2;
        leds[checkBound(animOriginPoint + aPos)] = animColor;
        leds[checkBound(animOriginPoint - aPos)] = animColor;
        leds[checkBound(animOriginPoint + aPos - 1)] = animColor;
        leds[checkBound(animOriginPoint - aPos + 1)] = animColor;
      }
      break;
    case 210: case 211:
      bool secondHalf = animFrame > NUM_LEDS / 2;
      int subFrame = secondHalf ? animFrame - (NUM_LEDS / 2) : animFrame;
      if(secondHalf)
      {
        fillColor(animColor);
      }
      for(int i = 0; i < subFrame; i++)
      {
        leds[checkBound(animOriginPoint + i)] = secondHalf ? neutralColor : animColor;
        leds[checkBound(animOriginPoint - i)] = secondHalf ? neutralColor : animColor;
      }
      break;
    case 212: case 213:
      bool partOne = animFrame < NUM_LEDS / 2;
      bool partTwo = !partOne && animFrame < (NUM_LEDS / 2) + 3;
      bool partThree = !partOne && !partTwo;
      if(partOne)
      {
        for(int i = 0; i < NUM_LEDS / 2; i++)
        {
          leds[checkBound(animOriginPoint + i)] = animColor;
          leds[checkBound(animOriginPoint - i)] = animColor;
        }
      } else if(partTwo || partThree)
      {
        fillColor(animColor);
        int skip = partThree ? 1 : 4 - (animFrame - (NUM_LEDS / 2));
        if(skip <= 0) skip = 1;
        
        int i = partThree ? animFrame % 4 : 0;
        while(i < NUM_LEDS)
        {
          leds[i] = neutralColor;
          i += skip;
        }
      }
      break;
      case 214:
        int otherCenter = checkBound(animOriginPoint + (NUM_LEDS / 2));
        if(animFrame < NUM_LEDS / 2)
        {
          fillColor(neutralColor);
          for(int i = 0; i < animFrame; i++)
          {
            leds[checkBound(animOriginPoint + i)] = animColor;
            leds[checkBound(animOriginPoint - i)] = animColor;
            leds[checkBound(otherCenter + i)] = animColor;
            leds[checkBound(otherCenter - i)] = animColor;
          }
        } else
        {
          fillColor(animColor);
          int offset = NUM_LEDS / 4;
          for(int i = 0; i < animFrame - (NUM_LEDS / 2); i++)
          {
            leds[checkBound(animOriginPoint + offset + i)] = neutralColor;
            leds[checkBound(animOriginPoint + offset - i)] = neutralColor;
            leds[checkBound(otherCenter + offset + i)] = neutralColor;
            leds[checkBound(otherCenter + offset - i)] = neutralColor;
          }
        }
        break;
  }
}

void fillColor(CRGB color)
{
  for(int i = 0; i < NUM_LEDS; i++)
    leds[i] = color;
}
void startAnim(int code)
{
  if(code < 200 || code < animCode)
    return; //anim codes with higher number can override current anims
  if(code > 201)
    return; //todo : fix the new anims
  animCode = code;
  animFrame = 0;
  shouldUpdateDisplay = true;
  switch(animCode)
  {
    case 200: case 201: case 202: case 203: case 204: case 205:
      animTick = 1 * animFrameMS;
      animOriginPoint = animCode % 2 == 0 ? blueCenter : redCenter;
      animColor = animCode %2 == 0 ? blueAnimColor : redAnimColor;
      break;
   case 210: case 211: case 212: case 213:
      animTick = 1 * animFrameMS;
      animOriginPoint = animCode % 2 == 0 ? blueCenter : redCenter;
      animColor = animCode %2 == 0 ? blueAnimColor : redAnimColor;
      break;
   case 214:
      animTick = 1 * animFrameMS;
      animOriginPoint = 0; //todo - need to store offset number on arduino to know where anim needs to end
      animColor = CRGB::DarkViolet;
      
      
  }
}
void advanceAnim()
{
  animFrame++;
  shouldUpdateDisplay = true;
  switch(animCode)
  {
    case 200: case 201:
      if(animFrame > 7)
        animCode = -1;
      else
      {
        animTick = (1 + animFrame) * animFrameMS;
        animColor = reduceBrightness(animColor, 255 - 32);
      }
      break;
    case 202: case 203:
      if(animFrame > 15)
        animCode = -1;
      else
      {
        animTick = 2 * animFrameMS;
        animColor = reduceBrightness(animColor, 255 - 32);
      }
      break;
    case 204: case 205:
      if(animFrame > 23)
        animCode = -1;
      else
      {
        animTick = 2 * animFrameMS;
        animColor = reduceBrightness(animColor, 255 - 32);
      }
      break;
    case 210: case 211:
      if(animFrame > NUM_LEDS)
        animCode = -1;
      else
      {
        animTick = 2 * animFrameMS;
      }
      break;
    case 212: case 213:
      animTick = 2 * animFrameMS;
      break;
    case 214:
      if(animFrame < (NUM_LEDS / 2))
        animTick = 1;
      else
      {
        int len = NUM_LEDS / 4;
        int totalTicks = animSpecial * 120;
        animTick = (totalTicks / len) * animFrameMS;
      }
      break;
  }
}
void setPaddleCenter(int playerID, int pixel)
{
  if(playerID == 0)
    blueCenter = pixel;
  else
    redCenter = pixel;
  if(shouldShowPaddles)
    shouldUpdateDisplay = true;
}
void setButtonLED(int pin, bool onOff)
{
  //todo - play with fades in SoftPWMSetFadeTime(pin, fadeInMS, fadeOutMS)
  SoftPWMSetPercent(pin, onOff ? buttonBrightness : 0);
}

int getCircularDistance(int from, int to)
{
  //todo - make this more efficient
  int dist = 0;
  int fromNeg = from;
  int fromPos = from;
  while(fromNeg != to && fromPos != to)
  {
    fromNeg = checkBound(fromNeg - 1);
    fromPos = checkBound(fromPos + 1);
    dist++;
  }
  if(fromPos == to)
    return dist;
  else
    return dist * -1;
}
void updateSparks()
{
  for(int i = 0; i < 3; i++)
  {
    bluePaddleDeltas[i+1] = bluePaddleDeltas[i];
    redPaddleDeltas[i+1] = redPaddleDeltas[i];
  }
  bluePaddleDeltas[0] = getCircularDistance(blueSparkLastCenter, blueCenter);
  redPaddleDeltas[0] = getCircularDistance(redSparkLastCenter, redCenter);
  blueSparkLastCenter = blueCenter;
  redSparkLastCenter = redCenter;
  //todo - implement red side after testing and tweaking on blue
  int bluePrime = (bluePaddleDeltas[0] - bluePaddleDeltas[1]);
  int bluePrimeOld = (bluePaddleDeltas[1] - bluePaddleDeltas[2]);
  int blueDoubleDelta =  bluePrime - bluePrimeOld;
  if(blueSparkTick >= 0)
  {
    blueSparkTick--;
    blueSparkPosition = checkBound(blueSparkPosition + blueSparkSpeed);
    shouldUpdateDisplay = true;
  }
  else if(abs(blueDoubleDelta) >= 3 && ((bluePrime >= 0 && bluePrimeOld <= 0) || (bluePrime <= 0 && bluePrimeOld >= 0)))
  {
    blueDoubleDelta = blueDoubleDelta * -1; //we want spark to go opposite of the direction change
    blueSparkPosition = checkBound(blueCenter + (blueDoubleDelta > 0 ? 2 : -2));
    blueSparkSpeed = (blueDoubleDelta > 0 ? 1 : -1);
    blueSparkTick = abs(blueDoubleDelta);
    shouldUpdateDisplay = true;
  } 
}
void loop() {
  
  for(int i = 0; i < 4; i++)
  {
    buttons[i]->loop();
    if(buttons[i]->IsPressed())
      buttons[i]->SetLight(true);
    if(buttons[i]->IsReleased())
      buttons[i]->SetLight(false);
    if(i < 2)
      spinners[i]->loop();
  } 
  
  //P1LeftButton.loop();
  //P1RightButton.loop();
  //if(P1LeftButton.IsPressed())
  //  P1LeftButton.SetLight(true);
  //  if(P1LeftButton.IsReleased())
  //  P1LeftButton.SetLight(false);
  //  P1LeftButton.loop();
   
  curTime = millis();
  int deltaTime = curTime - lastTime;
  if(deltaTime > 30000)
    deltaTime = 1; //edge case for number wrap
  nextSerialCountdown -= deltaTime;
  lastTime = curTime;
  if(nextSerialCountdown <= 0)
  {
    nextSerialCountdown += 10;
    if(Serial.availableForWrite())
    {
          byte p1ButtonData = 0b00000000;
          byte p2ButtonData = 0b01000000;
          byte p1SpinnerData = 0b10000000;
          byte p2SpinnerData = 0b11000000;
                  //  p1ButtonData += (P1RightButton.IsPressed() ? 0b00100000 : 0) + (P1RightButton.IsReleased() ? 0b00010000 : 0) + (P1RightButton.IsDown() ? 0b00001000 : 0);
        //p1 pressed always true for now, this way byte = 0 always means message complete
          p1ButtonData += (0b00100000) + (buttons[0]->IsReleased() ? 0b00010000 : 0) + (buttons[0]->IsDown() ? 0b00001000 : 0) + 
                  (buttons[1]->IsPressed() ? 0b00000100 : 0) + (buttons[1]->IsReleased() ? 0b00000010 : 0) + (buttons[1]->IsDown() ? 0b00000001 : 0);  
          p2ButtonData += (buttons[2]->IsPressed() ? 0b00100000 : 0) + (buttons[2]->IsReleased() ? 0b00010000 : 0) + (buttons[2]->IsDown() ? 0b00001000 : 0) + 
                  (buttons[3]->IsPressed() ? 0b00000100 : 0) + (buttons[3]->IsReleased() ? 0b00000010 : 0) + (buttons[3]->IsDown() ? 0b00000001 : 0);  
          int p1SpinDelta = spinners[0]->GetDeltaAndReset();
          if(p1SpinDelta < 0)
          {
            p1SpinDelta = p1SpinDelta * -1;
            p1SpinnerData += 0b00100000;
          }
          if(p1SpinDelta > 31)
            p1SpinDelta = 31;
          p1SpinnerData += p1SpinDelta;
         int p2SpinDelta = spinners[1]->GetDeltaAndReset();
          if(p2SpinDelta < 0)
          {
            p2SpinDelta = p2SpinDelta * -1;
            p2SpinnerData += 0b00100000;
          }
          if(p2SpinDelta > 31)
            p2SpinDelta = 31;
          p2SpinnerData += p2SpinDelta;          
        const byte buffer[4] = {p1ButtonData, p2ButtonData, p1SpinnerData, p2SpinnerData};
        
        if(DEBUG_LOG)
          Serial.println("p1 b:" + String(p1ButtonData) + " s:" + String(p1SpinnerData) + " p2 b:" + String(p2ButtonData) + " s:" + String(p2SpinnerData));
        else
          Serial.write(buffer, 4);
          
    }
    while(Serial.available())
    {
      byte bytes [5];
      byte message;
      Serial.readBytes(bytes, 1);
      message = bytes[0];
      
      bool validMessage = true;
      if(message >= 240)
      {
        //button and misc events
        switch(message)
        {
          case 240: setButtonLED(LED_P1LEFT, true); break;
          case 241: setButtonLED(LED_P1RIGHT, true); break;
          case 242: setButtonLED(LED_P2LEFT, true); break;
          case 243: setButtonLED(LED_P2RIGHT, true); break;
          case 244: setButtonLED(LED_P1LEFT, false); break;
          case 245: setButtonLED(LED_P1RIGHT, false); break;
          case 246: setButtonLED(LED_P2LEFT, false); break;
          case 247: setButtonLED(LED_P2RIGHT, false); break;
          case 252: case 253:
            Serial.readBytes(bytes, 1);
            if(message == 252)
              brightness = bytes[0];
            else
              buttonBrightness = bytes[0];
            EEPROM.write(1, brightness);
            EEPROM.write(2, buttonBrightness);
            setupColors();
            break;
        }
      }
    else if(message >= 200)
    {
      //special processing for some anims with extra data
      if(message == 214)
      {
        Serial.readBytes(bytes, 1);
        animSpecial = bytes[0];
      }
      startAnim(message);
    }
    else if(message >= 100)
      setPaddleCenter(1, message - 100);
    else
      setPaddleCenter(0, message);
  }
  if(shouldUpdateDisplay)
  {
    
    if(LED_STRIP_ENABLED)
    {
      setColors();
      FastLED.show();
    }
    shouldUpdateDisplay = false;
  }
  if(animCode > -1)
  {
    animTick -= deltaTime;
    if(animTick <= 0)
    {
      advanceAnim();
    }
  }
  }
}
 /*
  * Byte codes
  * 0 - 59 = blue paddle position update
  * 100 - 159 = red paddle position update
  * 200 / 201 = blue / red paddle anim hit
  */
