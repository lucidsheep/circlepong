#include <ezButton.h>
#include "FastLED.h"

class LEDButton
{
  private:
    ezButton button;
    int ledPin;
    bool down;
    bool pressed;
    bool released;
  public:
   LEDButton(int led_pin, int button_pin);
   void SetLight(bool isLit);
   void SetLight(int brightness);
   bool IsPressed();
   bool IsReleased(); 
   bool IsDown();
   void loop();
   void setup();

};
