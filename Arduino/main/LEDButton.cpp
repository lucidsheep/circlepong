#include <ezButton.h>
#include "FastLED.h"
#include <SoftPWM.h>
#include "LEDButton.h"
#include "Arduino.h"

  LEDButton::LEDButton(int led_pin, int button_pin) : button(button_pin)
  {
    ledPin = led_pin;
  }
  void LEDButton::SetLight(bool isLit)
  {
    SoftPWMSetPercent(ledPin, isLit ? 100 : 0); 
  }
  void LEDButton::SetLight(int brightness)
  {
    SoftPWMSetPercent(ledPin, brightness); 
  }
  bool LEDButton::IsPressed() { return pressed; }
  bool LEDButton::IsReleased()  { return released; }
  bool LEDButton::IsDown() { return down; }
  void LEDButton::loop()
  {
    button.loop();
    pressed = button.isPressed();
    released = button.isReleased();
    
    if(pressed) down = true;
    if(released) down = false;
    //Serial.println("btn null " + String(button.isPressed()) + String(down) + String(released));
  }
  void LEDButton::setup()
  {
    Serial.println("led pin is " + String(this -> ledPin));
      button.setCountMode(COUNT_FALLING);
      button.setDebounceTime(10);
      SetLight(0);
      down = false;
      pressed = false;
      released = false;
  }

