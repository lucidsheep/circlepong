﻿using UnityEngine;
using TMPro;
using DG.Tweening;

public class BlinkText : MonoBehaviour
{
    public int playerID = 0;
    public TextMeshPro text;
    public float blinkInterval = .4f;

    Tweener blinkAnim;

    private void Start()
    {
        CreditManager.instance.onCreditInserted.AddListener(OnCredit);
        GameEngine.instance.onStartPoint.AddListener(OnStartPoint);
        text.color = new Color(1f, 1f, 1f, 0f);
        GameEngine.instance.onModelChanged.AddListener(OnModel);
        OnModel();
    }

    void OnModel()
    {
        transform.localRotation = Quaternion.Euler(0f, 0f, (playerID == 0 ? GameEngine.p1Rotation : GameEngine.p2Rotation));
        transform.position = new Vector3(0f, -3.92f * (playerID == 0 ? 1f : -1f) * GameEngine.invertFloat, 0f);
    }
    void OnStartPoint()
    {
        EndMessage();
    }
    void OnCredit(int a, string b, bool readyToPlay)
    {
        EndMessage();
    }
    public void StartMessage(string message, float time = -1)
    {
        EndMessage();
        text.text = message;
        int loops = time < 0 ? -1 : Mathf.FloorToInt(time / blinkInterval);
        blinkAnim = text.DOColor(new Color(1f, 1f, 1f, 1f), blinkInterval).SetEase(Ease.Linear).SetLoops(loops, LoopType.Yoyo)
            .OnComplete(() => EndMessage());
    }
    
    void EndMessage()
    {
        Util.Maybe(blinkAnim, blink => blink.Kill());
        text.color = new Color(1f, 1f, 1f, 0f);
    }
}