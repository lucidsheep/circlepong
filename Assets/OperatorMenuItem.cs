﻿using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;
using System;

public class OperatorMenuItem : LSMenuItem
{

    public TextMeshPro label;
    public TextMeshPro item;
    public enum Type { P1Style, P2Style, CPUSpeed, Graphics, Restart, PointsToWin, Deuce, CoinsPerCredit, Debug, Close, FPS, WipeCredits, Invert, Exit, LEDOffset, CameraZoom, LEDBrightness}
    
    public enum DebugMode { Off = 0, FPS, Speed, DisplayData, InputState }
    LSEnum<DebugMode> debugModeList = new LSEnum<DebugMode>();

    public Type type;
    bool entered = false;
    public override void ReceiveCommand(LSMenu.Command command)
    {
        base.ReceiveCommand(command);
        if(command == LSMenu.Command.SELECT)
        {
            
            if (type == Type.Restart)
            {
                WebManager.GetNetworkConfig();
                parentMenu.CloseMenu();
                //SceneManager.LoadScene(0);
            }
            else if(type == Type.Close)
            {
                parentMenu.CloseMenu();
            } else if(type == Type.WipeCredits)
            {
                VTSaveManager.data.coinCount = 0;
                VTSaveManager.instance.SaveData();
            } else if(type == Type.Exit)
            {
                GameEngine.LaunchExternalApp("com.android.launcher3");
                parentMenu.CloseMenu();
            }
            else if (entered) Exit();
            else Enter();
        }
        if(entered)
        {
            if (command == LSMenu.Command.LEFT) Decrease();
            else if (command == LSMenu.Command.RIGHT) Increase(); 
        }
    }

    void Enter()
    {
        entered = true;
        item.color = Color.green;
    }
    void Exit()
    {
        entered = false;
        item.color = Color.white;
        VTSaveManager.instance.SaveData();
    }
    bool Decrease()
    {
        switch (type)
        {
            case Type.CPUSpeed: VTSaveManager.data.CPUSpeed -= 5; break;
            case Type.Debug: VTSaveManager.data.debug--; if (VTSaveManager.data.debug < 0) VTSaveManager.data.debug = debugModeList.typeArray.Length - 1 ; break;
            case Type.Deuce: VTSaveManager.data.deuce = !VTSaveManager.data.deuce; break;
            case Type.Graphics: VTSaveManager.data.graphicsQuality--; if (VTSaveManager.data.graphicsQuality < 0) VTSaveManager.data.graphicsQuality = 2; break;
            case Type.P1Style: VTSaveManager.data.p1CPU = !VTSaveManager.data.p1CPU; break;
            case Type.P2Style: VTSaveManager.data.p2CPU = !VTSaveManager.data.p2CPU; break;
            case Type.PointsToWin: VTSaveManager.data.pointsToWin--; break;
            case Type.FPS: if(VTSaveManager.data.fps > 5) VTSaveManager.data.fps -= 5; break;
            case Type.Invert: VTSaveManager.data.invertPlayers = !VTSaveManager.data.invertPlayers; break;
            case Type.LEDOffset: VTSaveManager.data.offset = Util.SetWithinBounds(VTSaveManager.data.offset - 1, 0, 80, true); break;
            case Type.CoinsPerCredit: VTSaveManager.data.coinsPerCredit = Mathf.Max(0, VTSaveManager.data.coinsPerCredit - 1); break;
            case Type.CameraZoom: VTSaveManager.data.cameraZoom = Util.SetWithinBounds(VTSaveManager.data.cameraZoom + .1f, 7.5f, 12.5f, false); break;
            case Type.LEDBrightness: VTSaveManager.data.LEDBrightness = Util.SetWithinBounds(VTSaveManager.data.LEDBrightness - 1, 0, 10, false); break;
            default: break;
        }
        SetItem();
        return true;
    }
    bool Increase()
    {
        switch (type)
        {
            case Type.CPUSpeed: VTSaveManager.data.CPUSpeed += 5; break;
            case Type.Debug: VTSaveManager.data.debug++; if (VTSaveManager.data.debug >= debugModeList.typeArray.Length) VTSaveManager.data.debug = 0; break;
            case Type.Deuce: VTSaveManager.data.deuce = !VTSaveManager.data.deuce; break;
            case Type.Graphics: VTSaveManager.data.graphicsQuality++; if (VTSaveManager.data.graphicsQuality >= 3) VTSaveManager.data.graphicsQuality = 0; break;
            case Type.P1Style: VTSaveManager.data.p1CPU = !VTSaveManager.data.p1CPU; break;
            case Type.P2Style: VTSaveManager.data.p2CPU = !VTSaveManager.data.p2CPU; break;
            case Type.PointsToWin: VTSaveManager.data.pointsToWin++; break;
            case Type.FPS: VTSaveManager.data.fps += 5; break;
            case Type.LEDOffset: VTSaveManager.data.offset = Util.SetWithinBounds(VTSaveManager.data.offset + 1, 0, 80, true); break;
            case Type.Invert: VTSaveManager.data.invertPlayers = !VTSaveManager.data.invertPlayers; break;
            case Type.CoinsPerCredit: VTSaveManager.data.coinsPerCredit++; break;
            case Type.CameraZoom: VTSaveManager.data.cameraZoom = Util.SetWithinBounds(VTSaveManager.data.cameraZoom - .1f, 7.5f, 12.5f, false); break;
            case Type.LEDBrightness: VTSaveManager.data.LEDBrightness = Util.SetWithinBounds(VTSaveManager.data.LEDBrightness + 1, 0, 10, false); break;
            default: break;
        }
        SetItem();
        return true;
    }
    public void Init(LSMenu parent, Type t)
    {
        Init(parent);
        type = t;
        SetLabel();
        SetItem();
    }
    public override void Init(LSMenu parent)
    {
        base.Init(parent);
    }

    public override void Select()
    {
        base.Select();
        label.color = Color.green;
    }

    public override void Deselect()
    {
        base.Deselect();
        label.color = Color.white;
    }
    void SetLabel()
    {
        string text = "";
        switch(type)
        {
            case Type.CoinsPerCredit: text = "Coins per Credit"; break;
            case Type.CPUSpeed: text = "CPU speed"; break;
            case Type.Deuce: text = "Deuce"; break;
            case Type.Graphics: text = "Quality"; break;
            case Type.Debug: text = "Debug"; break;
            case Type.P1Style: text = "Player 1"; break;
            case Type.P2Style: text = "Player 2"; break;
            case Type.PointsToWin: text = "Points"; break;
            case Type.Restart: text = "Update Network Config"; break;
            case Type.Close: text = "Close Menu"; break;
            case Type.FPS: text = "FPS"; break;
            case Type.WipeCredits: text = "Reset Credit Count"; break;
            case Type.Invert: text = "Reverse Players"; break;
            case Type.Exit: text = "Open Device Settings"; break;
            case Type.LEDOffset: text = "led offset"; break;
            case Type.CameraZoom: text = "Arena Size"; break;
            case Type.LEDBrightness: text = "LED Brightness"; break;
            default: text = "???"; break;
        }
        if (!Util.MultiComparison(type, Type.Close, Type.Restart, Type.WipeCredits, Type.Exit))
            text += ":";
        label.SetText(text);
    }

    void SetItem()
    {
        string text = "";
        switch (type)
        {
            case Type.CoinsPerCredit: text = VTSaveManager.data.coinsPerCredit == 0 ? "FREEPLAY" : VTSaveManager.data.coinsPerCredit.ToString(); break;
            case Type.CPUSpeed: text = VTSaveManager.data.CPUSpeed.ToString(); break;
            case Type.Deuce: text = VTSaveManager.data.deuce ? "On" : "Off"; break;
            case Type.Invert: text = VTSaveManager.data.invertPlayers ? "On" : "Off"; break;
            case Type.Graphics: text = VTSaveManager.data.graphicsQuality == 2 ? "High" : VTSaveManager.data.graphicsQuality == 1 ? "Med" : "Low"; break;
            case Type.Debug: text = debugModeList.stringArray[VTSaveManager.data.debug]; break;
            case Type.P1Style: text = VTSaveManager.data.p1CPU ? "CPU" : "Human"; break;
            case Type.P2Style: text = VTSaveManager.data.p2CPU ? "CPU" : "Human"; break;
            case Type.PointsToWin: text = VTSaveManager.data.pointsToWin.ToString(); break;
            case Type.Restart: case Type.Close: case Type.WipeCredits: text = ""; break;
            case Type.FPS: text = VTSaveManager.data.fps.ToString(); break;
            case Type.LEDOffset: text = VTSaveManager.data.offset.ToString(); break;
            case Type.CameraZoom: text = Mathf.FloorToInt((20f - VTSaveManager.data.cameraZoom) * 10f) + "%"; break;
            case Type.LEDBrightness: text = (VTSaveManager.data.LEDBrightness * 10) + "%"; break;
            default: text = ""; break;
        }
        item.SetText(text);
    }
}