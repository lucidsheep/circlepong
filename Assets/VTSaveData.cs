﻿[System.Serializable]
public class VTSaveData : SavedData
{
    int VERSION;

    public int graphicsQuality;
    public int pointsToWin;
    public bool p1CPU;
    public bool p2CPU;
    public int CPUSpeed;
    public bool deuce;
    public int coinsPerCredit;
    public int debug;
    public int fps;
    public int coinCount;
    public bool invertPlayers;
    public int offset;
    public float cameraZoom;
    public int LEDBrightness;
    public bool LEDInvert;
    public bool useNetworkConfig;

    /*
     * Version history
     * 0 - intial save data without version variable
     * 1 - added FPS defaulted to 60
     * 2 - added coinCoint defaulted to 0
     */
    override public int CURRENT_VERSION { get { return 7; }}

    override public bool UpdateSaveFile()
    {
        if (VERSION != CURRENT_VERSION)
        {
            if (VERSION < 1)
            {
                graphicsQuality = 2;
                pointsToWin = 10;
                p1CPU = p2CPU = false;
                CPUSpeed = 120;
                deuce = false;
                coinsPerCredit = 1;
                debug = 0;
                fps = 60;
                invertPlayers = false;
                offset = 0;
            }
            if (VERSION < 2)
            {
                coinCount = 0;
            }
            if (VERSION < 3)
            {
                cameraZoom = 10f;
            }
            if (VERSION < 4)
            {
                LEDBrightness = 10;
            }
            if (VERSION < 5)
            {
                fps = 999;
            }
            if (VERSION < 6)
            {
                LEDInvert = true;
            }
            if (VERSION < 7)
            {
                useNetworkConfig = true;
            }
            VERSION = CURRENT_VERSION;

            return true;
        }
        return false;
    }
}