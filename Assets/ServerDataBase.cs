﻿using UnityEngine;
[System.Serializable]
public class ServerDataBase
{
    public int status_code;
    public string status_detail;

    public string SerializeAsJson()
    {
        return JsonUtility.ToJson(this);
    }
}