﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO.Ports;
using System.IO;

public class ArduinoManager : MonoBehaviour
{
    public string portName = "COM5";
    public int baud = 9600;
    public int numLEDs = 80;
    public bool useManager = true;
    SerialPort stream;
    int LEDBrightness = 10;
    List<byte> buffer = new List<byte>();
    public static ArduinoManager instance;
    AndroidJavaClass jc;


    private void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        if (useManager)
        {
            if (!Application.isMobilePlatform)
            {
                stream = new SerialPort(portName, baud);
                try
                {
                    stream.Open();
                }
                catch (IOException e)
                {
                    useManager = false;
                }
            }
            else
            {
                jc = new AndroidJavaClass("com.lucid.circlepong.CustomActivity"); // somewhat expensive
            }
            if(useManager)
            {
                GameEngine.instance.onModelChanged.AddListener(OnModel);
                LEDBrightness = VTSaveManager.data.LEDBrightness;
            }
        }
    }

    void OnModel()
    {
        
        var delta = VTSaveManager.data.LEDBrightness - LEDBrightness;
        if(delta != 0 && useManager)
        {
            SendMessage(252);
            SendMessage(Util.SetWithinBounds(VTSaveManager.data.LEDBrightness * 26, 0, 255, false));
            LEDBrightness = VTSaveManager.data.LEDBrightness;
        }
    }
    // Update is called once per frame
    void Update()
    {
        /*
        if (VTButtons.GetLeftButtonDown(0)) SendMessage(240);
        if (VTButtons.GetRightButtonDown(0)) SendMessage(241);
        if (VTButtons.GetLeftButtonDown(1)) SendMessage(242);
        if (VTButtons.GetRightButtonDown(1)) SendMessage(243);
        if (VTButtons.GetLeftButtonUp(0)) SendMessage(244);
        if (VTButtons.GetRightButtonUp(0)) SendMessage(245);
        if (VTButtons.GetLeftButtonUp(1)) SendMessage(246);
        if (VTButtons.GetRightButtonUp(1)) SendMessage(247);
        */
        if (useManager)
        {
            int buttons = 0;
            if (Application.isMobilePlatform)
            {
                //jc.CallStatic("GetArduinoUpdate");
                buttons = jc.CallStatic<int>("GetButtonStates");
                VTButtons.ButtonUpdate(false, false, (buttons & 1) > 0);
                VTButtons.ButtonUpdate(false, true, (buttons & 2) > 0);
                VTButtons.ButtonUpdate(true, false, (buttons & 4) > 0);
                VTButtons.ButtonUpdate(true, true, (buttons & 8) > 0);
            }
            else
            {
                while (stream.BytesToRead > 0)
                {
                    int thisByte = stream.ReadByte();
                    if ((thisByte & 128) == 0)
                    {
                        //button
                        VTButtons.ButtonUpdate((thisByte & 64) > 0, false, (thisByte & 1) > 0);
                        VTButtons.ButtonUpdate((thisByte & 64) > 0, true, (thisByte & 8) > 0);
                    }
                    else
                    {
                        //spinners
                        bool negative = (thisByte & 32) > 0;
                        int val = (thisByte & 31) * (negative ? -1 : 1);
                        if ((thisByte & 64) > 0)
                            VTButtons.p2SpinDelta += val;
                        else
                            VTButtons.p1SpinDelta += val;
                    }
                }
            }
            
        }
        if (buffer.Count > 0)
        {
            if (Application.isMobilePlatform)
            {
                jc.CallStatic("ArduinoMessage", buffer.ToArray());
            }
            else
            {
                stream.Write(buffer.ToArray(), 0, buffer.Count);
            }
            buffer = new List<byte>();
        }
    }

    private void OnDestroy()
    {
        if(useManager && !Application.isMobilePlatform)
            stream.Close();
    }

    public static void OnPaddleMoved(int playerID, float rotation)
    {
        while (rotation < 0f) rotation += 360f;
        while (rotation > 360f) rotation -= 360f;
        if (VTSaveManager.data.LEDInvert)
            rotation = 360f - rotation;
        int message = playerID == 1 ? 100 : 0;
        message += (Util.SetWithinBounds(Mathf.FloorToInt(rotation / (360f / instance.numLEDs) + VTSaveManager.data.offset), 0, instance.numLEDs, true));
        SendMessage(message);
    }

    public static void OnPaddleHit(int playerID)
    {
        SendMessage(200 + playerID);
    }

    public static void SendMessage(params int[] messageCodes)
    {
        if (!instance.useManager) return;
        foreach (var messageCode in messageCodes)
        {
            if (messageCode >= 0 && messageCode <= 255)
                instance.buffer.Add((byte)messageCode);
        }

        
    }
}
