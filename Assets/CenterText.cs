﻿using UnityEngine;
using TMPro;
using DG.Tweening;

public class CenterText : MonoBehaviour
{
    public float rotationSpeed = 90f;
    TextMeshPro[] texts;
    Tweener anim;

    float _alpha = 0f;
    float alpha { get { return _alpha;  } set
        {
            _alpha = value;
            
            Util.Map(texts, x =>
            {
                //x.color = new Color(1f, 1f, 1f, value);
                x.fontMaterial.SetColor("_FaceColor", new Color(1f, 1f, 1f, value));
                //x.GetComponent<ntw.CurvedTextMeshPro.TextProOnACircle>().ForceUpdate();
            });
            var scale = .8f + (value * .2f);
            transform.localScale = new Vector3(scale, scale, 1f);
        } }
    private void Start()
    {
        texts = GetComponentsInChildren<TextMeshPro>();
        GameEngine.instance.onCenterText.AddListener(OnCenterText);
        transform.DORotate(new Vector3(0f, 0f, 360f), rotationSpeed, RotateMode.LocalAxisAdd).SetRelative(true).SetLoops(-1, LoopType.Incremental).SetEase(Ease.Linear);

    }

    private void Update()
    {
        //transform.rotation = Quaternion.Euler(0f, 0f, transform.rotation.z + (rotationSpeed * Time.deltaTime));
    }
    void OnCenterText(string text, int length = -1)
    {
        if (text == "")
        {
            ExitAnim();
            return;
        }
        Util.Maybe(anim, ja => ja.Complete());
        anim = DOTween.To(() => alpha, x => alpha = x, 1f, .5f);
        if (length < 0) length = text.Length;
        foreach (var txt in texts)
        {
            txt.SetText(text);
            txt.GetComponent<ntw.CurvedTextMeshPro.TextProOnACircle>().arcDegrees = 10f + (10f * length);
            txt.GetComponent<ntw.CurvedTextMeshPro.TextProOnACircle>().ForceUpdate();
        }
    }

    void ExitAnim()
    {
        Util.Maybe(anim, ja => ja.Complete());
        anim = DOTween.To(() => alpha, x => alpha = x, 0f, .5f);
    }
}