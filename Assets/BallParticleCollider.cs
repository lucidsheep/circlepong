﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallParticleCollider : MonoBehaviour
{
    public Ball ball;

    private void FixedUpdate()
    {
        GetComponent<Rigidbody2D>().MovePosition(ball.transform.position);
    }
}
