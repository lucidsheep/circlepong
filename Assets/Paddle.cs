﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Paddle : MonoBehaviour
{
    struct SpinFloat
    {
        float val;
        public static implicit operator float (SpinFloat ir)
        {
            return ir.val;
        }

        SpinFloat reduce()
        {
            float t = val % 360;
            if (t < 0f)
                t = 360f + t;
            return new SpinFloat(t);
        }
        float floatReduce(float v)
        {
            float t = v % 360;
            if (t < 0f)
                t = 360f + t;
            return t;
        }
        public static SpinFloat operator -(SpinFloat lhs, float rhs)
        {
            return new SpinFloat(lhs.val - rhs).reduce();
        }

        public static SpinFloat operator +(SpinFloat lhs, int rhs)
        {
            return new SpinFloat(lhs.val + rhs).reduce();
        }

        public SpinFloat(float a)
        {
            val = a;
            val = floatReduce(val);
        }

        public static float Distance(SpinFloat from, SpinFloat to)
        {
            if (from.val == to.val) return 0f;

            float posDis = 0f;
            if(to < from) // loop
            {
                float to360 = 360f - from;
                posDis = to360 + to.val;
            } else
            {
                posDis = to.val - from.val;
            }
            float negDis = 0f;
            if (from < to) // loop
            {
                float to360 = 360f - to;
                negDis = (from.val + to360);
            } else
            {
                negDis = from.val - to.val;
            }
            float ret = (posDis < negDis ? posDis : negDis * -1f);
            return ret;
        }
        public override string ToString()
        {
            return val.ToString();
        }
    }
    public int playerID = 0;
    public bool useKeys = false;
    public bool useAndroidSpinner = false;
    public bool useByteData = false;
    public bool isCPU = false;
    public bool invertMouse = false;
    public float sensitivity = 10f;
    public float chargeSensitivity = .5f;
    public float chargeRate = .004f;
    public float chargePower = 14f;
    public float smashPowerMultiplier = 1.2f;
    public float cpuSpeed = 10f;
    public int cpuCaptureFrames = 5;
    public PaddleObj paddle;
    public GameObject target;
    public GameObject spinTarget;
    public GameObject chargeBar;
    public SmashAttack smashAttackTemplate;

    float _spin = 0f;
    public float spin { get { return _spin;  } }
    public Vector2 spinVec { get { return spinVecAvg.average; } }

    LSRollingAverageVector2 spinVecAvg;
    int spinVecFrames = 3;

    float _chargeLevel = 0f;
    public float chargeLevel { get { return _chargeLevel; } set { _chargeLevel = value; chargeBar.transform.localScale = new Vector3(chargeBar.transform.localScale.x, (value), 1f); } }
    Vector3 lastSpinPosition;
    
    string axisToUse = "X";
    Vector2 lastMouse;
    float angle = 180f;
    float lastAngle;
    bool swingLock = false;
    Sequence swingSeq;
    
    List<Vector2> captureFrames;
    int curFrame = 0;
    float chargeFrames = 0f;
    bool isCharging = false;
    bool chargePositive = true;

    float lastAndroidMouseDelta = 0f;

    float freezeTime = 0f;
    float resetTime = 0f;
    float initialBounciness = 0f;
    Tweener paddleAnim;
    // Alexander Hamilton
    // My name is Alexander Hamilton
    // There's a million things I haven't done
    // But just you wait
    // Just you wait...
    // Start is called before the first frame update
    void Start()
    {
        lastMouse = Input.mousePosition;
        if (playerID == 1)
        {
            axisToUse = "Y";
        } 
        angle = (playerID == 0 ? GameEngine.p1Rotation : GameEngine.p2Rotation) + 90f;
        if(Application.isMobilePlatform)
        {
            useAndroidSpinner = true;
            useKeys = invertMouse = false;
        }
        lastSpinPosition = spinTarget.transform.position;
        initialBounciness = paddle.GetComponent<Rigidbody2D>().sharedMaterial.bounciness;
        spinVecAvg = new LSRollingAverageVector2(spinVecFrames);
        captureFrames = new List<Vector2>();
        for (int i = 0; i < cpuCaptureFrames; i++)
            captureFrames.Add(Vector2.zero);
        GameEngine.instance.onPossessionChange.AddListener(OnPossessionChange);
    }

    void OnNetworkConfig(NetworkGameConfigData configData)
    {
        if (!VTSaveManager.data.useNetworkConfig) return;
        if (configData.paddle_speed != 0f)
            sensitivity = configData.paddle_speed;
        if (configData.paddle_power != 0f)
            GetComponent<Rigidbody2D>().sharedMaterial.bounciness = configData.paddle_power;
        if (configData.paddle_charge_power != 0f)
            chargePower = configData.paddle_charge_power;
        if (configData.paddle_charge_speed != 0f)
            chargeRate = configData.paddle_charge_speed;
        if (configData.paddle_smash_multiplier != 0f)
            smashPowerMultiplier = configData.paddle_smash_multiplier;
    }
    void OnPossessionChange(int newOnwer)
    {
        //SetGlowLevel(newOnwer == playerID ? 0f : 1f, .1f);
        paddle.GetComponentInChildren<Collider2D>().enabled = newOnwer != playerID;
    }

    public void OnAndroidMouseDelta(Vector2 data)
    {
        lastAndroidMouseDelta = axisToUse == "X" ? data.x : data.y;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        //spinVec = target.transform.position - paddle.transform.position;
        //|| GameEngine.state != GameEngine.State.Gameplay
        if (freezeTime > 0f  || GameEngine.operatorMode) return;
        //GetComponent<Rigidbody2D>().MoveRotation(angle);
        //paddle.GetComponent<Rigidbody2D>().MovePosition(target.transform.position);
        //paddle.GetComponent<Rigidbody2D>().MoveRotation(angle);
        paddle.transform.rotation = Quaternion.Euler(0f, 0f, angle);
        spinVecAvg.AddValue(spinTarget.transform.position - lastSpinPosition);
        lastSpinPosition = spinTarget.transform.position;

    }

    public void StartFreeze(float fTime)
    {
        if (fTime <= 0f) return;

        freezeTime = fTime;
        Util.Maybe(swingSeq, justSeq =>
        {
            if (justSeq.IsPlaying()) 
                justSeq.Pause();
        });
    }
    public void StartReset()
    {
        resetTime = .5f;
    }
    private void Update()
    {
        float delta = (isCharging ? chargeSensitivity : sensitivity) * (playerID == 0 ? Input.mousePosition.x - lastMouse.x : Input.mousePosition.y - lastMouse.y);

        if (resetTime > 0f)
        {
            resetTime -= Time.deltaTime;
            SpinFloat neutralRotation = new SpinFloat((playerID == 1 ? GameEngine.p1Rotation : GameEngine.p2Rotation) - 90f);
            SpinFloat curRotation = new SpinFloat(angle);
            delta = Util.SetWithinBounds(SpinFloat.Distance(curRotation, neutralRotation), -10f, 10f, false);
        }
        else if (isCPU)
        {
            captureFrames[curFrame] = GameEngine.instance.ball.transform.position;
            if (GameEngine.instance.ball.possession == playerID)
                delta = 0f;
            else
            {
                var newFrame = captureFrames[curFrame];
                var oldFrame = curFrame == 0 ? captureFrames[cpuCaptureFrames - 1] : captureFrames[curFrame - 1];
                var movementVector = newFrame - oldFrame;
                Vector2 destination = GameEngine.instance.ball.transform.position;
                if (movementVector == Vector2.zero)
                    delta = 0f;
                else
                {
                    while (destination.magnitude < 3.75f)
                        destination += movementVector;
                    var lastJoystickDirection = destination;
                    float destAngle = Mathf.Atan2(lastJoystickDirection.y, lastJoystickDirection.x) * (180f / Mathf.PI);
                    //GameEngine.instance.debugTxt.SetText(destAngle.ToString() + " - " + angle.ToString());
                    SpinFloat sfDestAngle = new SpinFloat(destAngle);
                    SpinFloat sfAngle = new SpinFloat(angle);
                    delta = SpinFloat.Distance(sfAngle, sfDestAngle);
                    //GameEngine.instance.debugTxt.SetText(sfAngle.ToString() + "\n" + sfDestAngle.ToString() + "\n" + delta.ToString());

                    //float deltaBefore = delta;

                    delta = cpuSpeed * (delta >= 0f ? -1f : 1f) * Time.deltaTime;

                    //if (Mathf.Abs(deltaBefore) < Mathf.Abs(delta))
                    //    delta = deltaBefore;
                }
                //if (Mathf.Abs(delta) > cpuSpeed)
                //    delta = cpuSpeed * (delta >= 0f ? 1f : -1f);
                //delta *= Time.deltaTime;
            }
            curFrame = curFrame + 1 >= cpuCaptureFrames ? 0 : curFrame + 1;
        }
        else if (useAndroidSpinner || GameEngine.instance.fakeAndroidMouseInput)
        {
            delta = GameEngine.instance.GetAndroidMouseDelta(axisToUse == "X") * (isCharging ? chargeSensitivity : sensitivity) * -1f;//lastAndroidMouseDelta * sensitivity * .0166f; // * Time.deltaTime;
            lastAndroidMouseDelta = 0f;
            if (GameEngine.state != GameEngine.State.Gameplay) delta = 0f;
            //delta = sensitivity * (playerID == 0 ? Input.mousePosition.x - lastMouse.x : Input.mousePosition.y - lastMouse.y);
        }
        else if (useKeys)
        {
            delta = Input.GetKey(KeyCode.LeftArrow) ? -125f :
                Input.GetKey(KeyCode.RightArrow) ? 125f :
                0f;
            delta = delta * Time.deltaTime * (isCharging ? chargeSensitivity : sensitivity);
        }
        else if (useByteData)
        {
            if (playerID == 0)
            {
                delta = VTButtons.p1SpinDelta;
                VTButtons.p1SpinDelta = 0;
            }
            else
            {
                delta = VTButtons.p2SpinDelta;
                VTButtons.p2SpinDelta = 0;
            }
            delta = delta * -1f * .15f;
        }
        else
        {
            delta = sensitivity
                * Input.GetAxisRaw("Mouse " + axisToUse) * (invertMouse ? -1f : 1f) * (isCharging ? chargeSensitivity : sensitivity);
            if (GameEngine.state != GameEngine.State.Gameplay) delta = 0f;
        }

        if (freezeTime <= 0)
            angle += delta;
        else
        {
            freezeTime -= Time.deltaTime;
            if (freezeTime <= 0) // && swingSeq != null && !swingSeq.IsPlaying())
            {

            }
        }
        
        if (Mathf.Abs(angle) > 360f)
            angle = angle % 360f;
        _spin = delta;
        if (Mathf.Abs(angle - lastAngle) > 1f)
        {
            ArduinoManager.OnPaddleMoved(playerID, angle);
            lastAngle = angle;
        }
        lastMouse = Input.mousePosition;

        if (GameEngine.state != GameEngine.State.Gameplay || GameEngine.operatorMode)
        {
            //don't read button input outside of gameplay
            return;
        }
        if (VTButtons.GetAnyButtonDown(playerID) && !isCharging)
        {
            chargeLevel = chargeFrames = 0;
            chargePositive = isCharging = true;
            //paddle.GetComponent<Rigidbody2D>().sharedMaterial.bounciness = 0f;
        } else if(VTButtons.GetAnyButtonUp(playerID) && !VTButtons.GetAnyButton(playerID)) //only trigger smash if both buttons are up
        {
            //chargeLevel = chargeFrames = 0;
            //chargePositive = true;
            isCharging = false;
            //paddle.GetComponent<Rigidbody2D>().sharedMaterial.bounciness = initialBounciness;
            var smash = Instantiate(smashAttackTemplate, spinTarget.transform.position * .9f, paddle.transform.localRotation);
            smash.Init(chargeLevel < .25f ? 0f : chargeLevel, spinTarget.transform.position.normalized * -1f, this);
            //freezeTime = .5f;
            chargeLevel = chargeFrames = 0;
        }
        else if (VTButtons.GetAnyButton(playerID) && isCharging)
        {
            if (swingLock == false)
            {
                //chargeFrames += chargeRate * (chargePositive ? 1f : -1f) * (Time.deltaTime / .083333f);
                //if (chargeFrames <= 0f) chargeFrames = 1f;
            }
            if (chargePositive)
            {
                chargeLevel = Mathf.Min(chargeLevel + (chargeRate * Time.deltaTime), 1f);
                if (chargeLevel >= 1f)
                {
                    chargePositive = false;
                    chargeLevel = 1f;
                }
            }
            GridManager.instance.StartSuck(spinTarget.transform.position, playerID, chargeLevel);
        }
    }
}
