﻿using UnityEngine;
using DG.Tweening;

public class TitleText : MonoBehaviour
{
    public float rotSpeed = 6f;

    private void Start()
    {
        //DOTween.To(() => transform.localRotation.z, r => transform.localRotation = Quaternion.Euler(0f, 0f, r), -360f, 10f / rotSpeed).SetLoops(-1, LoopType.Incremental).SetEase(Ease.Linear);
        transform.DORotate(new Vector3(0f, 0f, 360f * (rotSpeed < 0f ? -1f : 1f)), Mathf.Abs(rotSpeed), RotateMode.LocalAxisAdd).SetRelative(true).SetLoops(-1, LoopType.Incremental).SetEase(Ease.Linear);
        GameEngine.instance.onGameStateChange.AddListener((os, ns) =>
        {
            if (ns == GameEngine.State.Pregame)
                gameObject.SetActive(true);
            else if (ns == GameEngine.State.Gameplay)
                gameObject.SetActive(false);
        });
    }
}