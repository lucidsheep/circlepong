﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundRing : MonoBehaviour
{
    public float start = -50f;
    public float end = 0f;
    public float speed = 10f;
    public bool loop = true;

    private void Update()
    {
        Util.EditTransform(gameObject, speed * Time.deltaTime, 4, true);
        if (transform.position.z > end)
        {
            if (loop)
                Util.EditTransform(gameObject, start, 4, false);
            else
                Destroy(this.gameObject);
        }
    }
}
