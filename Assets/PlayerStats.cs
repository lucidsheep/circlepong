﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStats
{
    public enum Type { Slam, Slice, Ace, Error, RallyLength, Hit, PointStart, PointEnd, Swing, Foul, Winner, Smash, Trick};
    public enum Tag { wonPoint, lostPoint, opponentID, wonGame, lostGame };

    public enum DataType { Int, Float, String };

    List<Tag> tagList = new List<Tag>();

    public int intVal = 0;
    public float floatVal = 0f;
    public string stringVal = "";
    public int playerID;
    public float timeStamp;
    public int pointID;

    public DataType dataType;
    public Type type;
    

    public PlayerStats(int id, Type etype, int val)
    {
        playerID = id;
        SetType(DataType.Int, etype);
        intVal = val;
    }

    public PlayerStats(int id, Type etype, float val)
    {
        playerID = id;
        SetType(DataType.Float, etype);
        floatVal = val;
    }


    public PlayerStats(int id, Type etype, string val)
    {
        playerID = id;
        SetType(DataType.String, etype);
        stringVal = val;
    }

    void SetType(DataType dtype, Type etype)
    {
        dataType = dtype;
        type = etype;
    }

    public override string ToString()
    {
        switch(dataType)
        {
            case DataType.Float: return floatVal.ToString();
            case DataType.Int: return intVal.ToString();
            case DataType.String: return stringVal;
        }
        return "?";
    }

    public PlayerStats AddTag(Tag tag)
    {
        if (!tagList.Contains(tag))
            tagList.Add(tag);
        return this;
    }
}
