﻿using UnityEngine;

public class PaddleLine : MonoBehaviour
{
    public LineRenderer lr;
    public GameObject paddle;
    public float speed = (60f / 450f);

    float startZ = .5f;
    int segments;
    private void Start()
    {
        segments = Mathf.FloorToInt(60f / speed);
        lr.positionCount = segments;
        for(int i = 0; i < segments; i++)
        {
            lr.SetPosition(i, new Vector3(paddle.transform.position.x, paddle.transform.position.y, startZ + (i*speed)));
        }
    }
    private void Update()
    {
        for(int i = segments - 1; i >= 1; i--)
        {
            var pos = lr.GetPosition(i - 1);
            pos.z += speed;
            lr.SetPosition(i, pos);
            lr.SetPosition(0, new Vector3(paddle.transform.position.x, paddle.transform.position.y, startZ));
        }
    }
}