﻿using UnityEngine;
using System.Collections;
using TMPro;
using UnityEngine.Events;
using DG.Tweening;
using System;

public class GameEngine : MonoBehaviour
{
    public enum State { Pregame, Gameplay, Postgame, Stats, Startup }
    public enum DebugData { FPS, BallSpeed }
    public enum PlayerButton { Left, Right }

    public static string gameTitle { get { return "vixi<rotate=180>v<rotate=0>"; } }
    public static float invertFloat { get { return VTSaveManager.data.invertPlayers ? -1f : 1f; } }
    public static float p1Rotation { get
        {
            return VTSaveManager.data.invertPlayers ? 180f : 0f;
        } }
    public static float p2Rotation { get { return VTSaveManager.data.invertPlayers ? 0f : 180f; } }
    public bool fakeAndroidMouseInput = false;
    public int scoreLimit;
    public float ballStartSpeed;
    public DebugData debugDisplay;
    public Texture2D cursorTexture;
    public PaddleObj p1Paddle;
    public PaddleObj p2Paddle;
    public Ball ball;
    public TextMeshPro debugTxt;
    public TextMeshPro comboTxt;
    public GameObject serveArrow;
    public GameObject ringAnim;
    public OperatorMenu menuTemplate;
    public BlinkText p1ServeTxt;
    public BlinkText p2ServeTxt;
    public PlayerData p1PlayerData = new PlayerData();
    public PlayerData p2PlayerData = new PlayerData();
    public GameData gameData;

    Tweener serveBlinkAnim;
    public static State state { get { return instance._state; } }

    public CenterTextEvent onCenterText = new CenterTextEvent();
    public AndroidMouseEvent onMouseEvent = new AndroidMouseEvent();
    public DesiredPossessionChangeEvent onPossessionChange = new DesiredPossessionChangeEvent();
    public OnGameStateChange onGameStateChange = new OnGameStateChange();
    public PointEndEvent onPointEnd = new PointEndEvent();
    public UnityEvent onStartPoint = new UnityEvent();
    public UnityEvent onModelChanged = new UnityEvent();
    public PlayerDataEvent onPlayerLogin = new PlayerDataEvent();
    public Vector2 AndroidMouseDelta = new Vector2();

    public static GameEngine instance;

    int p1score, p1seriesScore = 0;
    int p2score, p2seriesScore = 0;
    bool p1ready, p2ready = false;
    int curCombo = 0;
    State _state = State.Startup;
    bool menuOpen = false;
    LSMenu curMenu;
    bool p1Ready, p2Ready = false;
    int statMenusOpen = 0;
    bool readyForNextPoint = false;
    LSTimer forceNextPoint;
    string debugLog = "";
    int nextServePlayer = 0;

    public static bool operatorMode { get { return instance.menuOpen; } }
    // Use this for initialization

    private void Awake()
    {
        instance = this;
        VTSaveManager.instance = new VTSaveManager();
        VTSaveManager.instance.Init();
        WebManager.onServerUpdate.AddListener(OnServerUpdateData);
        //VTSaveManager.data.SetInitialData();
        //debugTxt.SetText("test");
    }
    void OnServerUpdateData(CheckUpdatesData data)
    {
        foreach(var p in data.players)
        {
            SetPlayerData(p, p.side_id);
            onPlayerLogin.Invoke(p, p.side_id);
        }
    }

    void SetPlayerData(PlayerData data, int position)
    {
        if (position == 0) p1PlayerData = data;
        else p2PlayerData = data;
    }

    void OnGameData(GameData data)
    {
        gameData = data;
    }
    void OnAndroidCreate(string data)
    {
        debugLog += data + "\n";
        if (VTSaveManager.data.debug == (int)OperatorMenuItem.DebugMode.DisplayData)
            debugTxt.text = debugLog;
    }
    void OnAndroidMouseMoved(string data)
    {
        var splitData = data.Split(','); //data[0] is x data, data[1] is y data
        float x = float.Parse(splitData[0]);
        float y = float.Parse(splitData[1]);
        AndroidMouseDelta = new Vector2(x, y);
        //debugTxt.SetText(data);
        p1Paddle.parent.OnAndroidMouseDelta(AndroidMouseDelta);
        p2Paddle.parent.OnAndroidMouseDelta(AndroidMouseDelta);
    }

    public int GetSeriesScore(int player) { return player == 0 ? p1seriesScore : p2seriesScore; }
    void Start()
    {
        //Cursor.visible = false; // Once Android cursor bug is fixed, you can delete the next 3 lines.
        //Cursor.lockState = CursorLockMode.Locked;
        UpdateModel();
        ball.onDesiredPossessionChange.AddListener(x => onPossessionChange.Invoke(x));
        //Texture2D cursorTexture = new Texture2D(32, 32);
        //cursorTexture.SetPixel(0, 0, Color.clear);
        //Cursor.SetCursor(cursorTexture, Vector2.zero, CursorMode.Auto);

        SetState(State.Pregame);
        CreditManager.instance.onCreditInserted.AddListener(OnCredit);
        StatManager.StartLogging();
        //StartNextPoint();
    }

    void OnCredit(int a, string b, bool ready)
    {
        if(ready && state == State.Pregame)
        onCenterText.Invoke("let's begin!", -1);
    }

    void StartDemo()
    {
        p1Paddle.parent.isCPU = p2Paddle.parent.isCPU = true;
        StartNextPoint();
    }
    void EndDemo()
    {
        UpdateModel();
    }
    void SetState(State newState)
    {
        var oldState = _state;
        if (oldState == newState) return;

        _state = newState;
        switch(newState)
        {
            case State.Pregame:
                onCenterText.Invoke((CreditManager.readyToPlay ? "tap to begin!" : "INSERT COIN"), -1);
                comboTxt.SetText("");
                StartDemo();
                break;
            case State.Gameplay:
                onCenterText.Invoke("", 0);
                EndDemo();
                StatManager.instance.OnGameStart();
                StartNextPoint();
                break;
            case State.Postgame:
                break;
            case State.Stats:
                statMenusOpen = 2;
                p1Ready = p2Ready = false;
                comboTxt.text = "";
                ArduinoManager.SendMessage(214, 30);
                break;


        }
        onGameStateChange.Invoke(oldState, newState);
    }

    public void StatScreenTimeout()
    {
        if (statMenusOpen > 0)
        {
            statMenusOpen = 0;
            p1Ready = p2Ready = false;
            p1seriesScore = p2seriesScore = 0;
            ResetGame();
            WebManager.StartNewGame();
        }
    }
    public void StatScreenExited(int playerID , bool wantsRematch)
    {
        if(wantsRematch)
        {
            if (playerID == 0) p1Ready = true;
            else p2Ready = true;
        }
        statMenusOpen--;
        if(statMenusOpen <= 0)
        {
            //ResetGame();
            if (p1Ready && p2Ready) //rematch is on
                Util.InvokeAfterDelay(() =>
                {
                    ResetGame();
                    if(CreditManager.StartGame())
                        SetState(State.Gameplay);
                }, .5f);
            else
            {
                p1seriesScore = p2seriesScore = 0;
                GameEngine.instance.gameData.set_id = 0;
                Util.InvokeAfterDelay(() => ResetGame(), .5f);
            }
            WebManager.StartNewGame();
        }
    }
    public int GetScore(int playerNum)
    {
        return playerNum == 0 ? p1score : playerNum == 1 ? p2score : 0;
    }
    public void OnScore(Ball ball, int player)
    {
        if(state == State.Pregame)
        {
            //demo mode
            TimeControl.StartTimer(.1f, StartNextPoint);
            //StartNextPoint();
            return;
        }
        if (player == -1) return;
        if (player == 0) p1score++;
        if (player == 1) p2score++;

        int pid = StatManager.instance.OnPointEnd(player);

        if(p1score >= scoreLimit || p2score >= scoreLimit)
        {
            onCenterText.Invoke((player == 0 ? "Blue " : "Red ") + "Wins!", -1);
            if (player == 0) p1seriesScore++;
            else p2seriesScore++;
            StatManager.instance.OnGameEnd(player);
            ArduinoManager.SendMessage(212 + player);
            SetState(State.Postgame);
        }
        else
        {
            TimeControl.StartTimer(1f, ReadyForNextPoint);
            ArduinoManager.SendMessage(210 + player);
            onCenterText.Invoke(ScoreDisplay.ScoreText(true), ScoreDisplay.ScoreText(false).Length);
            onPointEnd.Invoke(player, pid);
        }
        
    }
    void ReadyForNextPoint()
    {
        readyForNextPoint = true;
        var nextServe = nextServePlayer;
        if (nextServe == 0 ? p1Paddle.parent.isCPU : p2Paddle.parent.isCPU) StartNextPoint();
        else
        {
            BlinkText textToBlink = nextServe == 0 ? p1ServeTxt : p2ServeTxt;
            textToBlink.StartMessage("tap to serve");
            forceNextPoint = TimeControl.StartTimer(10f, StartNextPoint);
        }
    }
    void CheckButtonDown()
    {
        bool p1Down = VTButtons.GetAnyButtonDown(0);
        bool p2Down = VTButtons.GetAnyButtonDown(1);
        if (p1Down || p2Down)
        {
            if (state == State.Pregame)
            {
                if (CreditManager.readyToPlay)
                {
                    if (p1Down && !p1ready)
                    {
                        p1ready = true;
                        p1ServeTxt.StartMessage("ready!");
                    }
                    if (p2Down && !p2ready)
                    {
                        p2ready = true;
                        p2ServeTxt.StartMessage("ready!");
                    }
                    if (p1ready && p2ready)
                    {
                        p1ready = p2ready = false;
                        CreditManager.StartGame();
                        SetState(State.Gameplay);
                    }
                }
                else
                {
                    if (p1Down)
                        p1ServeTxt.StartMessage("insert credit", 3f);
                    if (p2Down)
                        p2ServeTxt.StartMessage("insert credit", 3f);
                }

            }
            else if (state == State.Gameplay)
            {
                if (readyForNextPoint)
                {
                    if (nextServePlayer == 0 && p1Down) StartNextPoint();
                    else if (nextServePlayer == 1 && p2Down) StartNextPoint();
                }
            }
            else if (state == State.Postgame)
            {
                SetState(State.Stats);
            }
            else if (state == State.Stats)
            {
                //ResetGame();
            }
        }
    }

    void StartNextPoint()
    {
        Util.Maybe(forceNextPoint, x => TimeControl.RemoveTimer(x.id));
        readyForNextPoint = false;
        nextServePlayer = ball.Reset();
        p1Paddle.parent.StartReset();
        p2Paddle.parent.StartReset();
        if(state == State.Gameplay)
        {
            onCenterText.Invoke("", -1);
            IncreaseCombo(true);
            StatManager.instance.OnPointStart();
            onStartPoint.Invoke();
        }
    }

    void OnMenuClosed()
    {
        Time.timeScale = 1f;
        menuOpen = false;
        UpdateModel();
    }

    void UpdateModel()
    {
        scoreLimit = VTSaveManager.data.pointsToWin;
        p1Paddle.parent.isCPU = VTSaveManager.data.p1CPU;
        p2Paddle.parent.isCPU = VTSaveManager.data.p2CPU;
        p1Paddle.parent.cpuSpeed = p2Paddle.parent.cpuSpeed = VTSaveManager.data.CPUSpeed;
        GridManager.instance.SetQuality(VTSaveManager.data.graphicsQuality);
        if (VTSaveManager.data.debug == (int)OperatorMenuItem.DebugMode.DisplayData)
            OnAndroidCreate(GetDisplayData());
        Application.targetFrameRate = VTSaveManager.data.fps;
        onModelChanged.Invoke();
    }
    // Update is called once per frame
    void Update()
    {
        CheckButtonDown();
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            if (menuOpen)
            {
                curMenu.CloseMenu();
            }
            else
            {
                var menu = Instantiate(menuTemplate);
                curMenu = menu;
                menuOpen = true;
                Time.timeScale = 0f;
                menu.SetupMenu(true);
                menu.onMenuClosed.AddListener(OnMenuClosed);
            }
        }
        if (VTSaveManager.data.debug != 0)
        {
            
            switch (VTSaveManager.data.debug)
            {
                case (int)OperatorMenuItem.DebugMode.FPS:
                    debugTxt.text = Mathf.FloorToInt(FPSCalc.fps).ToString();
                    break;
                case (int)OperatorMenuItem.DebugMode.Speed:
                    debugTxt.text = ball.GetComponent<Rigidbody2D>().velocity.magnitude.ToString();
                    break;
                case (int)OperatorMenuItem.DebugMode.InputState:
                    debugTxt.text = VTButtons.ButtonStateToString();
                    break;
            }
        }
        else
        {
            debugTxt.text = "";
        }
    }
    string GetDisplayData()
    {
        string ret = "";
        using (AndroidJavaClass jc = new AndroidJavaClass("com.lucid.circlepong.CustomActivity")) // somewhat expensive
        {
            ret = jc.CallStatic<string>("GetDisplayModeOptions");
        }
        return ret;
    }

    public static void LaunchExternalApp(string appName)
    {
        using (AndroidJavaClass jc = new AndroidJavaClass("com.lucid.circlepong.CustomActivity")) // somewhat expensive
        {
            jc.CallStatic("StartAndroidApp", appName);
        }
    }
    public float GetAndroidMouseDelta(bool xDelta = true)
    {
        float ret = 0f;
        using (AndroidJavaClass jc = new AndroidJavaClass("com.lucid.circlepong.CustomActivity")) // somewhat expensive
        {
            ret = xDelta ? jc.CallStatic<float>("GetXDelta") : jc.CallStatic<float>("GetYDelta");
        }
        if(fakeAndroidMouseInput)
        {
            ret += Input.GetAxisRaw("Mouse " + (xDelta ? "X" : "Y")) * 20f;
        }
        if (xDelta)
            onMouseEvent.Invoke(ret, 0f);
        else
            onMouseEvent.Invoke(0f, ret);
        return ret;
    }
    void ResetGame()
    {
        p1score = p2score = 0;
        curCombo = 0;
        SetState(State.Pregame);
    }
    public void SetServeArrow(Vector2 serveVector)
    {
        serveArrow.GetComponentInChildren<SpriteRenderer>().enabled = true;
        float angle = Mathf.Atan2(serveVector.y, serveVector.x) * (180f / Mathf.PI);
        serveArrow.transform.rotation = Quaternion.Euler(0f, 0f, angle);
    }

    public void HideServeArrow()
    {
        serveArrow.GetComponentInChildren<SpriteRenderer>().enabled = false;

    }

    public void IncreaseCombo(bool reset = false)
    {
        if (state != State.Gameplay) return;

        if (reset)
            curCombo = 0;
        else
        { 
            curCombo++;
            //Instantiate(ringAnim, Vector3.zero, Quaternion.Euler(0f, 90f, 0f));
        }
        comboTxt.SetText(curCombo >= 3 ? curCombo.ToString(): "");
    }


}
