﻿using UnityEngine;
using System.Collections;
using TMPro;

public class ScoreDisplay : MonoBehaviour
{
    public int playerID;
    public bool showBothPlayers = false;
    public TextMeshPro text;

    public static string ScoreText(bool colored = true, bool blueFirst = true, bool spacing = false)
    {
        string blueText = GameEngine.instance.GetScore(0).ToString();
        string redText = GameEngine.instance.GetScore(1).ToString();
        string midText = spacing ? " - " : "-";
        if(colored)
        {
            redText = "<color=#FF0000>" + redText;
            blueText = "<color=#00FFFF>" + blueText;
            midText = "<color=#FFFFFF>" + midText;
        }
        return blueFirst ?
            blueText + midText + redText :
            redText + midText + blueText;
    }
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (!showBothPlayers)
            text.SetText(GameEngine.instance.GetScore(playerID).ToString());
        else if (GameEngine.state == GameEngine.State.Stats) text.SetText("");
        else
        {
            text.SetText(ScoreText(true, playerID == 0, true));
        }
    }
}
