﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using DG.Tweening;


public class StatScreen : MonoBehaviour
{
    public int playerID;
    public TextMeshPro
        titleTxt,
        matchTimeTxt,
        dataTxt,
        labelTxt,
        qrTxt,
        seriesTxt,
        yesTxt,
        noTxt,
        scoreTxt,
        rematchTxt,
        creditsTxt;
    public SpriteRenderer qrCode;
    public SpriteRenderer bg;
    public GameObject resultsTxtParent;
    public GameObject rematchTxtParent;
    public TMP_FontAsset highlightMaterial;
    public TMP_FontAsset unhighlightMaterial;
    float _globalAlpha = 1f;
    Tweener curAnim;
    TextMeshPro[] resultsAllText;
    TextMeshPro[] rematchAllText;

    int screenState = -1; //-1 = hidden, 0 = results, 1 = rematch


    public float globalAlpha
    {
        get
        {
            return _globalAlpha;
        }
        set
        {
            _globalAlpha = value;
            Util.Map(resultsAllText, t => t.color = new Color(t.color.r, t.color.g, t.color.b, value));
            Util.Map(rematchAllText, t => t.color = new Color(t.color.r, t.color.g, t.color.b, 1f - value));
            qrCode.color = new Color(1f, 1f, 1f, value);
            //Util.Map(GetComponentsInChildren<SpriteRenderer>(), t => t.color = new Color(t.color.r, t.color.g, t.color.b, value));
        }
    }

    public int playerServerID { get { return playerID == 0 ? GameEngine.instance.p1PlayerData.player_id : GameEngine.instance.p2PlayerData.player_id; } }
    // Start is called before the first frame update
    void Start()
    {
        //globalAlpha = 0f;
        //transform.localScale = new Vector3(1.25f, 1.25f, 1f);
        rematchTxtParent.transform.localPosition = new Vector3(.5f, 0f, 0f);
        GameEngine.instance.onGameStateChange.AddListener(OnStateChange);
        GameEngine.instance.onMouseEvent.AddListener(OnAndroidMouse);
        GameEngine.instance.onModelChanged.AddListener(OnModelChange);
        GameEngine.instance.onPlayerLogin.AddListener(OnCheckUpdates);
        CreditManager.instance.onCreditInserted.AddListener((_,__,___) => OnCredit(___));
        resultsAllText = resultsTxtParent.GetComponentsInChildren<TextMeshPro>();
        rematchAllText = rematchTxtParent.GetComponentsInChildren<TextMeshPro>();
        globalAlpha = 1f;
        gameObject.SetActive(false);
        OnModelChange();
    }

    void OnCheckUpdates(PlayerData pd, int sideID)
    {
        UpdateQRCode();
    }
    void OnModelChange()
    {
        var rotation = playerID == 0 ? GameEngine.p1Rotation : GameEngine.p2Rotation;
        transform.localRotation = Quaternion.Euler(0f, 0f, rotation);
        if (screenState == -1)
            Util.EditTransform(gameObject, 6f * (playerID == 0 ? -1f : 1f) * GameEngine.invertFloat, 2);
    }
    void OnCredit(bool canPlay = false)
    {
        creditsTxt.text = CreditManager.isFreeplay ? "freeplay" : "credits: " + CreditManager.creditsString;
        yesTxt.font = CreditManager.readyToPlay ? highlightMaterial : unhighlightMaterial;
        noTxt.font = highlightMaterial;
    }
    void OnAndroidMouse(float xDelta, float yDelta)
    {
        if (screenState != 1) return;
    }
    void Update()
    {
        if (screenState < 0) return;
        if (VTButtons.GetLeftButtonDown(playerID)) AdvanceScreen(true);
        else if (VTButtons.GetRightButtonDown(playerID)) AdvanceScreen(false);
    }

    void OnStateChange(GameEngine.State oldState, GameEngine.State newState)
    {
        if (oldState == GameEngine.State.Stats) HideScreen();
        else if (newState == GameEngine.State.Stats) ShowScreen();
    }

    public void ShowScreen()
    {
        gameObject.SetActive(true);
        float animTime = .5f;
        //curAnim = DOTween.To(() => globalAlpha, x => globalAlpha = x, 1f, animTime);
        curAnim = transform.DOMoveY(0f, animTime).SetEase(Ease.OutExpo);
        //transform.DOScale(1f, animTime);
        UpdateText();
        UpdateQRCode();
        Util.Map(rematchAllText, t => t.color = new Color(t.color.r, t.color.g, t.color.b, 0f));
        screenState = 0;
    }

    void AdvanceScreen(bool leftButton)
    {
        //if (curAnim != null && !curAnim.IsComplete()) return;
        if (isAnimating) return;

        if(screenState == 0)
        {
            curAnim = DOTween.To(() => globalAlpha, x => globalAlpha = x, 0f, .5f);
            resultsTxtParent.transform.DOLocalMoveX(-.5f, .5f);
            rematchTxtParent.transform.DOLocalMoveX(0f, .5f);
            screenState = 1;
        } else if(screenState == 1)
        {
            bool wantsRematch = false;
            if (leftButton && CreditManager.readyToPlay)
            {
                wantsRematch = true;
            }
            else if (leftButton)
                return; //deny button press with 0 credits
            GameEngine.instance.StatScreenExited(playerID, wantsRematch);
            HideYesNo();
            rematchTxt.text = wantsRematch ? "Ready" : "Game over";
            screenState = 2;
        }
    }

    void HideYesNo()
    {
        yesTxt.text = noTxt.text = "";
        /*
        if (!wantsRematch)
            yesTxt.text = "";
        else
            noTxt.text = "";
            */
    }
    void UpdateText()
    {
        int points = 0;
        int smashShots = 0;
        int hits = 0;
        int slams = 0;
        int slices = 0;
        int winners = 0;
        int fouls = 0;
        int aces = 0;
        int trickShots = 0;
        float topSlam = 0f;

        int i = playerID;
        points = StatManager.allStats.FindAll(x => x.playerID == i && x.type == PlayerStats.Type.PointEnd).Count;
        hits = StatManager.allStats.FindAll(x => x.playerID == i && x.type == PlayerStats.Type.Hit).Count;
        var allSlams = StatManager.allStats.FindAll(x => x.playerID == i && x.type == PlayerStats.Type.Slam);
        slams = allSlams.Count;
        fouls = StatManager.allStats.FindAll(x => x.playerID == i && x.type == PlayerStats.Type.Foul).Count;
        winners = StatManager.allStats.FindAll(x => x.playerID == i && x.type == PlayerStats.Type.Winner).Count;
        slices = StatManager.allStats.FindAll(x => x.playerID == i && x.type == PlayerStats.Type.Slice).Count;
        trickShots = StatManager.allStats.FindAll(x => x.playerID == i && x.type == PlayerStats.Type.Trick).Count;
        smashShots = StatManager.allStats.FindAll(x => x.playerID == i && x.type == PlayerStats.Type.Smash).Count;

        var allRallies = StatManager.allStats.FindAll(x => x.type == PlayerStats.Type.RallyLength);
        int count = allRallies.Count;
        int totalRallies = Util.Fold(allRallies, (cur, total) => total += cur.intVal, 0);
        allRallies.Sort((x, y) => x.intVal.CompareTo(y.intVal));
        int longestRally = allRallies[allRallies.Count - 1].intVal;
        float avgRally = Util.Round((float)totalRallies / (float)count, 2);
        bool didWin = points >= VTSaveManager.data.pointsToWin;

        titleTxt.SetText((didWin ? "Victory" : "Defeat"));
        matchTimeTxt.SetText(" " + Util.FormatTime(StatManager.totalGameTime));

            dataTxt.SetText(
                longestRally + "\n" //longest rally
                + avgRally + "\n" //avg rally
                + trickShots + "\n" //trick shots
                + slams + "\n" //power shots
                + smashShots + "\n" //smashes
                + slices + "\n" //slices
                + fouls + "\n" //fouls
                + aces + "\n" //aces
                );
        seriesTxt.SetText("series score " + GameEngine.instance.GetSeriesScore(playerID) + "-" + GameEngine.instance.GetSeriesScore(playerID == 0 ? 1 : 0));
        scoreTxt.SetText(ScoreDisplay.ScoreText(false, playerID == 0, true));
        OnCredit();
    }
    void UpdateQRCode()
    {
        qrCode.enabled = playerServerID == 0;
        qrCode.sprite = LSQRCode.GenerateQRCode("https://vixia.cab/beta/join.php?cabid=" + 1 + "&gameid=" + GameEngine.instance.gameData.game_id + "&sideid=" + playerID, LSQRCode.ColorMode.WhiteOnBlack);
        qrTxt.text = playerServerID == 0 ? "Scan to make a profile and     track your     stats!" : "game saved\nto your\nprofile!";
    }
    public void HideScreen()
    {
        float animTime = .5f;
        //curAnim = DOTween.To(() => globalAlpha, x => globalAlpha = x, 0f, animTime);
        curAnim = transform.DOMoveY(6f * (playerID == 0 ? -1f : 1f) * GameEngine.invertFloat, animTime).SetEase(Ease.InExpo).OnComplete(() => ResetScreen());
        //transform.DOScale(new Vector3(1.25f, 1.25f, 1f), animTime);
    }
    
    void ResetScreen()
    {
        resultsTxtParent.transform.localPosition = Vector3.zero;
        rematchTxtParent.transform.localPosition = new Vector3(.5f, 0f, 0f);
        globalAlpha = 1f;
        screenState = -1;
        yesTxt.text = "yes";
        noTxt.text = "no";
        rematchTxt.text = "rematch?";
        gameObject.SetActive(false);
    }
    public bool isAnimating
    {
        get
        {
            return curAnim != null && curAnim.IsPlaying();
        }
    }
}
