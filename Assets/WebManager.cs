﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System;
using System.Text;
using System.IO;
using System.Security.Cryptography;

public class WebManager : MonoBehaviour
{
    public bool useManager;
    public string cabID;
    public string password;
    public string secretKey;
    static WebManager instance;
    bool connected = false;

    public static ServerUpdateEvent onServerUpdate = new ServerUpdateEvent();
    public static NetworkConfigEvent onNetworkConfigUpdate = new NetworkConfigEvent();
    private void Awake()
    {
        instance = this;
    }
    void Start()
    {
        if (useManager)
            StartCoroutine(Login());
    }
    void MaybeCheckUpdates()
    {
        if ((GameEngine.instance.p1PlayerData.player_id == 0 || GameEngine.instance.p2PlayerData.player_id == 0 ) && Util.MultiComparison(GameEngine.state, GameEngine.State.Pregame, GameEngine.State.Stats, GameEngine.State.Gameplay))
            StartCoroutine(CheckUpdates());
    }
    public static void OnGameEnd()
    {
        instance.StartCoroutine(instance.GameEnd());
    }

    public static void GetNetworkConfig()
    {
        instance.StartCoroutine(instance.NetworkConfigUpdate());
    }

    IEnumerator NetworkConfigUpdate()
    {
        UnityWebRequest request = UnityWebRequest.Get("https://vixia.cab/beta/test.php?id=" + cabID + "&func=config");
        yield return request.SendWebRequest();

        if (request.isNetworkError || request.isHttpError)
        {
            Debug.Log("DB connection error: " + request.error);
        }
        else
        {
            var ret = JsonUtility.FromJson<NetworkGameConfigData>(DecryptString(request.downloadHandler.text));
            onNetworkConfigUpdate.Invoke(ret);
            Debug.Log("Network config received");
        }
    }
    IEnumerator GameEnd()
    {
        var json = GameEngine.instance.gameData.SerializeAsJson();
        Debug.Log(json);
        var encrypted = EncryptString(json);
        Debug.Log(encrypted);
        UnityWebRequest request = UnityWebRequest.Get("https://vixia.cab/beta/test.php?id=" + cabID + "&func=endgame&input=" + EncryptString(json));
        yield return request.SendWebRequest();

        if (request.isNetworkError || request.isHttpError)
        {
            Debug.Log("DB connection error: " + request.error);
        }
        else
        {
            //Debug.Log(request.downloadHandler.text);
            var ret = JsonUtility.FromJson<ServerDataBase>(DecryptString(request.downloadHandler.text));
            Debug.Log("Endgame server code " + ret.status_code + ", description: " + ret.status_detail);
        }
    }
    IEnumerator Login()
    {
        UnityWebRequest request = UnityWebRequest.Get("https://vixia.cab/beta/login.php?id=" + cabID + "&pw=" + password);
        yield return request.SendWebRequest();

        if (request.isNetworkError || request.isHttpError)
        {
            Debug.Log("DB connection error: " + request.error);
        }
        else
        {
            var result = request.downloadHandler.text;
            if (result.Length > 0)
            {
                connected = true;
                Debug.Log("DB connection success");
                StartCoroutine(CheckUpdates());
                StartCoroutine(StartGame());
                StartCoroutine(NetworkConfigUpdate());
                /*
                var encrypted = EncryptString("testing123");
                Debug.Log(secretKey + " encrypted is " + encrypted);
                var decrypted = DecryptString(encrypted);
                Debug.Log("...and decrypted back is " + decrypted);
                */
            } else
            {
                Debug.Log("DB refused to connect. Check cab ID and password");
            }
        }
    }

    IEnumerator CheckUpdates()
    {
        UnityWebRequest request = UnityWebRequest.Get("https://vixia.cab/beta/test.php?id=" + cabID + "&func=checkupdates");
        yield return request.SendWebRequest();

        if (request.isNetworkError || request.isHttpError)
        {
            Debug.Log("DB connection error: " + request.error);
        }
        else
        {
            var result = request.downloadHandler.text;
            var json = JsonUtility.FromJson<CheckUpdatesData>(DecryptString(result));
            if(json != null)
                onServerUpdate.Invoke(json);
            /*
            var player = json.players[0];
            Debug.Log("Update found! Player id: \n" + player.player_id + " sk: " + player.secret_key);
            Debug.Log("json reencode: \n" + json.SerializeAsJson());
            */
        }
        StartCoroutine(CheckUpdates()); //start next long poll
    }
    public static void StartNewGame()
    {
        instance.StartCoroutine(instance.StartGame());
    }
    IEnumerator StartGame()
    {
        var sendData = new StartGameData();
        sendData.players = new int[]{ 0, 0 };
        if (GameEngine.instance.p1PlayerData != null) sendData.players[0] = GameEngine.instance.p1PlayerData.player_id;
        if (GameEngine.instance.p2PlayerData != null) sendData.players[1] = GameEngine.instance.p2PlayerData.player_id;
        sendData.set_id = GameEngine.instance.gameData.set_id;
        var url = "https://vixia.cab/beta/test.php?id=" + cabID + "&func=startgame&input=" + EncryptString(sendData.SerializeAsJson());
        Debug.Log("Start game: " + url);
        UnityWebRequest request = UnityWebRequest.Get(url);
        yield return request.SendWebRequest();

        if (request.isNetworkError || request.isHttpError)
        {
            Debug.Log("DB connection error: " + request.error);
        }
        else
        {
            Debug.Log("Game Data received: " + request.downloadHandler.text);
            var gameData = JsonUtility.FromJson<GameData>(DecryptString(request.downloadHandler.text));
            GameEngine.instance.gameData = gameData;
        }
    }
    string EncryptString(string input)
    {
        try
        {
            // Create Aes that generates a new key and initialization vector (IV).    
            // Same key must be used in encryption and decryption    
            using (AesManaged aes = new AesManaged())
            {
                using (SHA256 sha256Hash = SHA256.Create())
                {
                    var key = Encoding.UTF8.GetBytes(secretKey); // sha256Hash.ComputeHash(Encoding.UTF8.GetBytes(secretKey));

                    //pad key out to 32 bytes (256bits) if its too short
                    if (key.Length < 32)
                    {
                        var paddedkey = new byte[32];
                        Buffer.BlockCopy(key, 0, paddedkey, 0, key.Length);
                        key = paddedkey;
                    }

                    aes.Mode = CipherMode.ECB;
                    aes.Key = key;
                    byte[] encrypted;
                    // Encrypt string    
                    ICryptoTransform encryptor = aes.CreateEncryptor();
                    // Create MemoryStream    
                    using (MemoryStream ms = new MemoryStream())
                    {
                        // Create crypto stream using the CryptoStream class. This class is the key to encryption    
                        // and encrypts and decrypts data from any given stream. In this case, we will pass a memory stream    
                        // to encrypt    
                        using (CryptoStream cs = new CryptoStream(ms, encryptor, CryptoStreamMode.Write))
                        {
                            // Create StreamWriter and write data to a stream    
                            using (StreamWriter sw = new StreamWriter(cs))
                                sw.Write(input);
                            encrypted = ms.ToArray();
                        }
                    }
                    return ByteArrayToString(encrypted, true);
                }
            }
        }
        catch (Exception exp)
        {
            Console.WriteLine(exp.Message);
        }
        return "";
}
    string DecryptString(string input)
    {
        byte[] cipherText = StringToByteArray(input);
        string ret = "";
        try
        {
            // Create Aes that generates a new key and initialization vector (IV).    
            // Same key must be used in encryption and decryption    
            using (AesManaged aes = new AesManaged())
            {
                using (SHA256 sha256Hash = SHA256.Create())
                {
                    aes.Mode = CipherMode.ECB;
                    var key = Encoding.UTF8.GetBytes(secretKey); // sha256Hash.ComputeHash(Encoding.UTF8.GetBytes(secretKey));

                    //pad key out to 32 bytes (256bits) if its too short
                    if (key.Length < 32)
                    {
                        var paddedkey = new byte[32];
                        Buffer.BlockCopy(key, 0, paddedkey, 0, key.Length);
                        key = paddedkey;
                    }
                    aes.Key = key;
                    // Encrypt string    
                    ICryptoTransform decryptor = aes.CreateDecryptor();
                    // Create MemoryStream    
                    using (MemoryStream ms = new MemoryStream(cipherText))
                    {
                        // Create crypto stream    
                        using (CryptoStream cs = new CryptoStream(ms, decryptor, CryptoStreamMode.Read))
                        {
                            // Read crypto stream    
                            using (StreamReader reader = new StreamReader(cs))
                                ret = reader.ReadToEnd();
                        }
                    }
                }
            }
        }
        catch (Exception exp)
        {
            Console.WriteLine(exp.Message);
        }
        return ret;
    }
    public static string ByteArrayToString(byte[] ba, bool escape = true)
    {
        if (escape)
            return UnityWebRequest.EscapeURL(Convert.ToBase64String(ba), Encoding.UTF8);
        else
            return Convert.ToBase64String(ba);
        /*
        StringBuilder hex = new StringBuilder(ba.Length * 2);
        foreach (byte b in ba)
            hex.AppendFormat("{0:x2}", b);
        return hex.ToString();
        */
    }
    public static byte[] StringToByteArray(string hexString, bool unescape = false)
    {
        if (unescape)
            return Convert.FromBase64String(UnityWebRequest.UnEscapeURL(hexString));
        else
            return Convert.FromBase64String(hexString);
        /*
        byte[] data = new byte[hexString.Length / 2];
        for (int index = 0; index < data.Length; index++)
        {
            string byteValue = hexString.Substring(index * 2, 2);
            data[index] = byte.Parse(byteValue, System.Globalization.NumberStyles.HexNumber);
        }

        return data;
        */
    }

    static string Decrypt(byte[] cipherText, byte[] Key, byte[] IV)
    {
        string plaintext = null;
        // Create AesManaged    
        using (AesManaged aes = new AesManaged())
        {
            // Create a decryptor    
            ICryptoTransform decryptor = aes.CreateDecryptor(Key, IV);
            // Create the streams used for decryption.    
            using (MemoryStream ms = new MemoryStream(cipherText))
            {
                // Create crypto stream    
                using (CryptoStream cs = new CryptoStream(ms, decryptor, CryptoStreamMode.Read))
                {
                    // Read crypto stream    
                    using (StreamReader reader = new StreamReader(cs))
                        plaintext = reader.ReadToEnd();
                }
            }
        }
        return plaintext;
    }  

    // Update is called once per frame
    void Update()
    {
        
    }
}
