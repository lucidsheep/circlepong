﻿[System.Serializable]
public class GameStatData : ServerDataBase
{
    public int game_id;
    public int player_id;
    public int side_id;
    public int points;
    public int result_type;
    public int shots_slice;
    public int shots_power;
    public int shots_smash;
    public int shots_ace;
    public int shots_fault;
}