﻿using UnityEngine;

public class OperatorMenu : LSMenu
{
    public OperatorMenuItem itemTemplate;
    public OperatorMenuItem.Type[] menu;
    bool inItem = false;

    public override void Setup()
    {
        base.Setup();
        int pos = 0;
        foreach(var t in menu)
        {
            var item = Instantiate(itemTemplate, transform);
            item.Init(this, t);
            item.menuPosition = new Vector2(0f, pos);
            item.transform.localPosition = new Vector3(0f, 2.8f - (item.menuPosition.y * .6f), -2f);
            itemList.Add(item);

            pos++;
        }
        SetItem(itemList[0]);
    }

    protected override bool GetCommand(Command command)
    {
        if (command == Command.SELECT && (currentItem as OperatorMenuItem).type != OperatorMenuItem.Type.WipeCredits && base.GetCommand(command)) inItem = !inItem;
        if (inItem && (command == Command.UP || command == Command.DOWN)) return false;

        return base.GetCommand(command);
    }

}