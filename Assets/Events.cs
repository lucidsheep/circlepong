﻿using UnityEngine.Events;

public class DesiredPossessionChangeEvent : UnityEvent<int> { }
public class CenterTextEvent : UnityEvent<string, int> { }

public class AndroidMouseEvent : UnityEvent<float, float> { }

public class OnGameStateChange : UnityEvent<GameEngine.State, GameEngine.State> { }

public class PlayerButtonPressEvent : UnityEvent<int, GameEngine.PlayerButton> { }

public class PointEndEvent : UnityEvent<int, int> { }

public class CreditInsertedEvent : UnityEvent<int, string, bool> { }

public class ServerUpdateEvent : UnityEvent<CheckUpdatesData> { }
public class NetworkConfigEvent : UnityEvent<NetworkGameConfigData> { }
public class PlayerDataEvent : UnityEvent<PlayerData, int> { }