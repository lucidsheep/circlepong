﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using DG.Tweening;

public class CountdownBar : MonoBehaviour
{
    public GameObject bar;
    public TextMeshPro p1Txt, p2Txt;
    LSTimer timer;
    bool running = false;
    // Start is called before the first frame update
    void Start()
    {
        GameEngine.instance.onGameStateChange.AddListener(OnStateChange);
        Hide();
    }

    void OnStateChange(GameEngine.State before, GameEngine.State after)
    {
        if(after == GameEngine.State.Stats)
        {
            Util.InvokeAfterDelay(Init, .1f);
        } else if(before == GameEngine.State.Stats)
        {
            if (running)
                Hide();
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(running)
        {
            bar.transform.localScale = new Vector3(1f - timer.progress, 1f, 1f);
            p1Txt.text = p2Txt.text = Mathf.FloorToInt(timer.duration + 1f).ToString();
        }
    }

    public void Hide()
    {
        p1Txt.text = p2Txt.text = "";
        bar.transform.localScale = new Vector3(0f, 1f, 1f);
        running = false;
        Util.Maybe(timer, t => TimeControl.RemoveTimer(t.id));
    }

    public void Init()
    {
        bar.transform.DOScaleX(1f, .9f).SetEase(Ease.OutExpo).OnComplete(() =>
        {
            timer = TimeControl.StartTimer(30f, OnTimeExpired);
            running = true;
        });
    }

    void OnTimeExpired()
    {
        if (!running) return;
        running = false;
        GameEngine.instance.StatScreenTimeout();
        Hide();
    }
}
