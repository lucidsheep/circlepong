﻿using UnityEngine;
using System.Collections.Generic;
public class TickerTest : MonoBehaviour
{
    public LSTicker[] tickers;

    float idleTime = 0f;
    int lastIdleMessage = -1;
    private void Start()
    {
        WelcomeMessage();
        GameEngine.instance.onGameStateChange.AddListener(OnGameState);
        GameEngine.instance.onPointEnd.AddListener(OnPointEnd);
        CreditManager.instance.onCreditInserted.AddListener((a,b,c) => OnCredit(c));
    }

    public delegate void TickerDelegate(LSTicker t);
    void Message(TickerDelegate tickerFunc)
    {
        foreach(var ticker in tickers)
        {
            tickerFunc(ticker);
        }
    }

    void OnCredit(bool nowReadyToPlay)
    {
        Message(t =>
        {
            t.ResetTicker(true);
            t.Add("credits: " + CreditManager.creditsString).SetMode(LSTicker.Mode.StopCenter).SetSpeed(999f);
            t.AddDelay(2f);
            t.AddClear();
        });
    }
    void PostPointCommentary(int pointID)
    {
        /*
         * 0 - foul
         * 1 - 30+ rally
         * 2 - smash
         * 3 - generic nice shot
         */
        var pointStats = StatManager.GetPointData(pointID);
        LSWeightedList<int> commentaryOptions = new LSWeightedList<int>();

        if (pointStats.FindAll(x => x.type == PlayerStats.Type.Foul).Count > 0)
            commentaryOptions.Add(0, 50);
        if (pointStats.FindAll(x => x.type == PlayerStats.Type.Hit).Count >= 30)
            commentaryOptions.Add(1, 30);
        if (pointStats.FindAll(x => x.type == PlayerStats.Type.Winner).Count > 0)
            commentaryOptions.Add(2, 50);
        commentaryOptions.Add(3, 10);
        int chosenMessage = commentaryOptions.GetRandomItem();
        switch(chosenMessage)
        {
            case 0:
                Message(t => t.Add("fault!").SetEase(DG.Tweening.Ease.OutBack).SetMode(LSTicker.Mode.StopCenter)); break;
            case 1:
                Message(t => t.AddLetters("epic rally!", 16f, DG.Tweening.Ease.InCirc).SetMode(LSTicker.Mode.StopLeft)); break;
            case 2:
                Message(t => t.AddLetters("smaaaaaaaaaash!", 24f, DG.Tweening.Ease.InCirc).SetMode(LSTicker.Mode.StopLeft)); break;
            case 3:
                Message(t => t.AddLetters("nice shot!", 16f, DG.Tweening.Ease.InCirc).SetMode(LSTicker.Mode.StopLeft)); break;

        }
    }

    void OnPointEnd(int playerID, int pointID)
    {
        Message(t => t.ResetTicker(true));
        PostPointCommentary(pointID);
        Message(t => t.AddClear());
        Message(t => t.AddDelay(.5f));
        string statusMessage = playerID == 0 ? "Blue " : "Red ";
        int scoringPlayerScore = GameEngine.instance.GetScore(playerID);
        int otherPlayerScore = GameEngine.instance.GetScore(1 - playerID);
        int diff = scoringPlayerScore - otherPlayerScore;
        if (scoringPlayerScore + 1 == VTSaveManager.data.pointsToWin)
            statusMessage += "is on game point!";
        else if (diff >= 3)
            statusMessage += "is domininating!";
        else if (diff == 2)
            statusMessage += "is in control!";
        else if (diff == 1)
            statusMessage += "takes the lead!";
        else if (diff == 0)
            statusMessage += "ties it up!";
        else if (diff == -1)
            statusMessage += "makes it a one point game!";
        else if (diff == -2)
            statusMessage += "is closing the gap!";
        else if (diff <= -3)
            statusMessage += "is still in this!";
        Message(t => t.Add(statusMessage).SetMode(LSTicker.Mode.Once));
        Message(t => t.Add(ScoreDisplay.ScoreText(true, true, true)).SetMode(LSTicker.Mode.StopCenter));
    }

    void IdleCommentary()
    {
        /*
        * 0 - insert credit
        * 1 - free play
        * 2 - neo cade
        * 3 - shy tease
        */
        LSWeightedList<int> commentaryOptions = new LSWeightedList<int>();

        //todo - go to free play line if enough coins are inserted
        if(CreditManager.readyToPlay)
            commentaryOptions.Add(1, 50);
        else
            commentaryOptions.Add(0, 50);
        commentaryOptions.Add(2, 20);
        commentaryOptions.Add(3, 10);

        int chosenMessage = -1;
        do
        {
            chosenMessage = commentaryOptions.GetRandomItem();
        } 
        while (chosenMessage == lastIdleMessage);
        lastIdleMessage = chosenMessage;
        switch (chosenMessage)
        {
            case 0:
                Message(t => t.Add("insert coin to play").SetMode(LSTicker.Mode.Once)); break;
            case 1:
                Message(t => t.Add("press a button to start!").SetMode(LSTicker.Mode.Once)); break;
            case 2:
                Message(t => t.Add("a neocade machine").SetMode(LSTicker.Mode.Once)); break;
            case 3:
                Message(t => t.Add("don't be shy! i don't bite :)").SetMode(LSTicker.Mode.Once)); break;
        }
    }
    void Update()
    {
        var ticker = tickers[0];
        if (ticker.isReadyForText && GameEngine.state == GameEngine.State.Pregame) idleTime += Time.deltaTime;
        else idleTime = 0f;

        if(idleTime > 3f)
        {
            IdleCommentary();
            idleTime = 0f;
        }
    }
    void OnGameState(GameEngine.State old, GameEngine.State newState)
    {
        if(newState == GameEngine.State.Pregame)
        {
            Message(t => t.ResetTicker(true));
            IdleCommentary();
        }
        else if (newState == GameEngine.State.Gameplay)
        {
            Message(t =>
            {
                t.Add("good luck and have fun!").SetMode(LSTicker.Mode.Once);
                t.Add(ScoreDisplay.ScoreText(true, true, true)).SetMode(LSTicker.Mode.StopCenter);
            });
        } else if(newState == GameEngine.State.Postgame)
        {
            Message(t =>
            {
                t.ResetTicker(true);
                t.AddLetters(GameEngine.instance.GetScore(0) == VTSaveManager.data.pointsToWin ? "Blue wins!" : "Red wins!", 16f, DG.Tweening.Ease.InCirc).SetMode(LSTicker.Mode.StopCenter);
                t.AddDelay(2f);
                t.AddClear();
                t.Add("good game! final score: ").SetMode(LSTicker.Mode.Once);
                t.Add(ScoreDisplay.ScoreText(true, true, true)).SetMode(LSTicker.Mode.StopCenter);
            });
        } else if(newState == GameEngine.State.Stats)
        {
            Message(t => t.ResetTicker(true));
            StatsMessage();
        }
    }

    void StatsMessage()
    {
        var totalPoints = StatManager.allStats.FindAll(x => x.type == PlayerStats.Type.PointEnd).Count;
        var totalHits = StatManager.allStats.FindAll(x => x.type == PlayerStats.Type.Hit).Count;
        var allSlams = StatManager.allStats.FindAll(x => x.type == PlayerStats.Type.Slam);

        var allRallies = StatManager.allStats.FindAll(x => x.type == PlayerStats.Type.RallyLength);
        int count = allRallies.Count;
        int totalRallies = Util.Fold(allRallies, (cur, total) => total += cur.intVal, 0);
        allRallies.Sort((x, y) => x.intVal.CompareTo(y.intVal));
        int longestRally = allRallies[allRallies.Count - 1].intVal;
        float avgRally = Util.Round((float)totalRallies / (float)count, 2);

        string message = "final score: " + ScoreDisplay.ScoreText(true, true, true) + "<color=#FFFFFF>   game time: " + Util.FormatTime(StatManager.totalGameTime) + "   total hits: " + totalHits + "   longest rally: " + longestRally + "   avg rally: " + avgRally;
        Message(t => t.Add(message).SetMode(LSTicker.Mode.Repeat).SetEase(DG.Tweening.Ease.Linear).SetSpeed(2f));
    }
    void WelcomeMessage()
    {
        Message(t =>
        {
            t.Add("Welcome to "+GameEngine.gameTitle).SetMode(LSTicker.Mode.StopLeft);
            t.AddDelay(2f);
            t.AddClear();
        });
        //Add("Welcome to Vortex  ");
        //Add("Welcome to Vortex  ");
        //Add(ScoreDisplay.ScoreText(true, true, true)).SetMode(Mode.StopCenter);
    }
}