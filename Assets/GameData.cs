﻿[System.Serializable]
public class GameData : ServerDataBase
{
    public int cab_id;
    public int game_id;
    public int set_id;
    public string start_date;
    public float average_rally;
    public int longest_rally;
    public int season_id;
    public float game_time;
    public int game_complete;
    public GameStatData[] game_players;
}