﻿using UnityEngine;
using DG.Tweening;

public class SmashAttack : MonoBehaviour
{
    public SpriteRenderer sprite;
    public Collider2D smashCollider;
    public Collider2D mainCollider;
    public float power;
    public float smashMultiplier = 2f;

    public float magnitude;
    float timeLeft;
    public Vector2 vector;

    public Paddle parent;
    bool startDestroy = false;

    public void Init(float m, Vector2 v, Paddle p)
    {
        magnitude = m;
        timeLeft = .1f;
        vector = v;
        parent = p;
        this.power = p.chargePower;
        smashMultiplier = p.smashPowerMultiplier;
        sprite.transform.DOLocalMoveX(0f, timeLeft).SetEase(Ease.Linear);
        sprite.transform.DOScaleX(1f, timeLeft).SetEase(Ease.Linear);
    }

    private void Update()
    {
        if (!startDestroy && timeLeft <= 0f)
        {
            startDestroy = true;
            Util.Map(GetComponentsInChildren<Collider2D>(), x => x.enabled = false);
            sprite.DOColor(new Color(1f, 1f, 1f, 0f), .1f).SetEase(Ease.Linear).OnComplete(() => Destroy(this.gameObject));//Destroy(this.gameObject);
        }
        timeLeft -= Time.deltaTime;
    }

    public Vector2 GetForce()
    {
        return vector * (1f + magnitude) * power;
    }
    public Vector2 GetSmashForce()
    {
        return GetForce() * smashMultiplier;
    }

    public bool IsTapShot()
    {
        return magnitude <= .05f;
    }
}