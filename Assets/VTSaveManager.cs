﻿using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System;
using System.Collections.Generic;
using System.Globalization;
public class VTSaveManager : SaveManager<VTSaveData>
{

    public override void Init()
    {
        base.Init();
        LoadData();
    }

    override public void LoadData()
    {
        base.LoadData();
        if (File.Exists(Application.persistentDataPath + "/save.gd"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/save.gd", FileMode.Open);
            _data = (VTSaveData)bf.Deserialize(file);
            file.Close();
            if (_data.UpdateSaveFile())
                SaveData();
        }
        else
        {
            ResetData();
        }
    }

    override public void SaveData()
    {
        base.SaveData();
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/save.gd");
        bf.Serialize(file, data);
        file.Close();
    }


}