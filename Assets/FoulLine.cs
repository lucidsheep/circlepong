﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class FoulLine : MonoBehaviour
{
    public Ball ball;
    public Color[] colors;
    float initScale;
    //void Awake()
    void Start()
    {
        ball.onDesiredPossessionChange.AddListener(OnPossessionChange);
        initScale = transform.localScale.x;
    }

    void OnPossessionChange(int newOnwer)
    {
        transform.DOKill(true);
        transform.DOScale(initScale * 1.05f, .05f).SetLoops(2, LoopType.Yoyo).SetEase(Ease.OutQuad);
        //GetComponent<SpriteRenderer>().DOKill();
        //GetComponent<SpriteRenderer>().DOColor(colors[newOnwer], .05f).SetLoops(2, LoopType.Yoyo).SetEase(Ease.OutQuad);
    }
    //void Update()
    //void FixedUpdate()
    //void LateUpdate()
    //void OnDestroy()
}
