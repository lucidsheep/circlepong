﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Ball : MonoBehaviour
{
    public float startSpeed = 5f;
    public float maxSpeed { get { return minSpeed * 4f; } }
    public float minSpeed = 10f;
    public float speedIncrement = .1f;
    public float spinMagnitude = 30f;
    public float spinAngle = 10f;
    public GameObject neutralBody, redBody, blueBody;
    public Light gridGlowLight;
    public Color[] lightColors;
    public Color[] gridColors;
    public GameObject[] onCollisionAnim;
    public GameObject[] onScoreAnim;
    public GameObject[] onConversionAnim;

    public DesiredPossessionChangeEvent onDesiredPossessionChange = new DesiredPossessionChangeEvent();
    public int possession { get { return curPossession; } }
    int curPossession = -1;
    int desiredPossession = -1;
    int lastServe = -1;
	int resetFrames = 0;
    float freezeTime = 0f;
    public float initialDrag = 0f;
    Vector3 freezePos;

    float initialMinSpeed;
    int hits = 0;
    Vector2 spinVec = Vector2.zero;
    float spinVecV2 = 0f;
    public Vector2 getSpin { get { return spinVec; } }

    Vector3 posLastFrame;
    // Start is called before the first frame update
    void Start()
    {
        initialMinSpeed = minSpeed;
        initialDrag = GetComponent<Rigidbody2D>().drag;
        WebManager.onNetworkConfigUpdate.AddListener(OnNetworkConfigData);
    }

    private void OnNetworkConfigData(NetworkGameConfigData data)
    {
        if (VTSaveManager.data.useNetworkConfig == false) return;
        if (data.ball_speed != 0f)
            initialMinSpeed = data.ball_speed;
        if (data.ball_drag != 0f)
            GetComponent<Rigidbody2D>().drag = data.ball_drag;
        if (data.ball_spin != 0f)
            spinMagnitude = data.ball_spin;
    }

    // Update is called once per frame
    void Update()
    {
        //GameEngine.instance.debugTxt.SetText(GetComponent<Rigidbody2D>().velocity.ToString());
        if(freezeTime > 0f)
        {
            transform.position = freezePos;
            freezeTime -= Time.deltaTime;
            return;
        }
		if(resetFrames > 0)
		{
			resetFrames--;
			return;
		}
        if(desiredPossession != curPossession && transform.position.magnitude < 2.5f)
        {
            curPossession = desiredPossession;
            SetBody();
            onDesiredPossessionChange.Invoke(curPossession);
            Instantiate(onConversionAnim[curPossession], transform); // new Vector3(transform.position.x, transform.position.y, -2f), transform.rotation);
        }
        posLastFrame = transform.position;
    }

    private void FixedUpdate()
    {
        
        if(freezeTime > 0f)
        {
            GetComponent<Rigidbody2D>().drag = 0f;
            return;
        }
        GetComponent<Rigidbody2D>().drag = initialDrag;

        AddSpinForce();

        if (GetComponent<Rigidbody2D>().velocity.magnitude < minSpeed)
        {
            GetComponent<Rigidbody2D>().velocity = (GetComponent<Rigidbody2D>().velocity.normalized * (minSpeed));
        }

        //AddSpinForce();

        if (GetComponent<Rigidbody2D>().velocity.magnitude > maxSpeed)
        {
            GetComponent<Rigidbody2D>().velocity = (GetComponent<Rigidbody2D>().velocity.normalized * (maxSpeed));
        }
        

    }

    void AddSpinForce()
    {
        /* //this did not work great but maybe there's potential somewhere
        var deg = spinVecV2;
        var v = GetComponent<Rigidbody2D>().velocity.normalized;
        var newV = new Vector2(
            (Mathf.Cos(deg) * v.x) - (Mathf.Sin(deg) * v.y),
            (Mathf.Sin(deg) * v.x) + (Mathf.Cos(deg) * v.y));

        var magBefore = GetComponent<Rigidbody2D>().velocity.magnitude;
        GetComponent<Rigidbody2D>().AddForce(newV * spinMagnitude);
        GetComponent<Rigidbody2D>().velocity = Vector2.ClampMagnitude(GetComponent<Rigidbody2D>().velocity + (newV * spinMagnitude), magBefore);
        */

        var magBefore = GetComponent<Rigidbody2D>().velocity.magnitude;
        GetComponent<Rigidbody2D>().AddForce(spinVec, ForceMode2D.Impulse);
        GetComponent<Rigidbody2D>().velocity = Vector2.ClampMagnitude(GetComponent<Rigidbody2D>().velocity, magBefore); //spin vec should never increase overall velocity
        
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
		if(resetFrames > 0) return;
        //GetComponent<Rigidbody2D>().AddTorque(200f);
        if(collision.gameObject.GetComponent<PaddleObj>() != null)
        {
            var paddleObj = collision.gameObject.GetComponent<PaddleObj>();
            //todo - try additive spinVec (don't reset it on each hit)
            spinVec = paddleObj.spinVec * -1f * spinMagnitude * collision.relativeVelocity.magnitude;
            spinVecV2 = paddleObj.spin * spinAngle; // * spinMagnitude;
            //Debug.Log("spin mag " + spinVec.magnitude);
            

            //Debug.Log(spinVec.magnitude.ToString());
            if (spinVec.magnitude > .25f)
            {
                StatManager.AddEvent(new PlayerStats(desiredPossession, PlayerStats.Type.Slice, spinVec.magnitude));
            }

            PostHit(collision.relativeVelocity, paddleObj.parent, .075f);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "Tipper")
        {
            bool isSmash = false;
            bool isTap = false;
            var smashData = collision.gameObject.GetComponentInParent<SmashAttack>();
            if(!GetComponent<Rigidbody2D>().IsTouching(smashData.mainCollider) && smashData.magnitude >= .95f) 
            {
                isSmash = true;
                StatManager.AddEvent(new PlayerStats(smashData.parent.playerID, PlayerStats.Type.Smash, 1f));
                ArduinoManager.SendMessage(204 + smashData.parent.playerID);
                Debug.Log("Smash!");
            }else if (smashData.IsTapShot())
            {
                Debug.Log("Tap");
                isTap = true;
                StatManager.AddEvent(new PlayerStats(smashData.parent.playerID, PlayerStats.Type.Trick, 1f));
                spinVec *= 2f;
            }
            else
            {
                Debug.Log("Hit " + smashData.magnitude);
                if (smashData.magnitude >= .75f)
                {
                    StatManager.AddEvent(new PlayerStats(smashData.parent.playerID, PlayerStats.Type.Slam, 1f));
                    ArduinoManager.SendMessage(202 + smashData.parent.playerID);
                }
            }
            if (isTap)
            {
                GetComponent<Rigidbody2D>().velocity = GetComponent<Rigidbody2D>().velocity * -1f;
                PostHit(Vector3.zero, smashData.parent, 0f);
            }else
            {
                var force = isSmash ? smashData.GetSmashForce() : smashData.GetForce();
                float forceMultiplier = Mathf.Max(1f, GetComponent<Rigidbody2D>().velocity.magnitude / initialMinSpeed);
                GetComponent<Rigidbody2D>().velocity = new Vector2(); //smash hit cancels current velocity
                GetComponent<Rigidbody2D>().AddForce(force * forceMultiplier, ForceMode2D.Impulse);
                PostHit(force, smashData.parent, isSmash ? .15f : .10f);
            }
        }
    }

    void PostHit(Vector3 hitMagnitude, Paddle paddle, float fTime = .1f)
    {
        desiredPossession = paddle.playerID;

        Instantiate(onCollisionAnim[desiredPossession], transform.position, transform.rotation);

        Vector3 cameraImpact = hitMagnitude;
        cameraImpact *= .05f;
        cameraImpact = Vector3.ClampMagnitude(cameraImpact, 1f);
        cameraImpact.z = -10f;
        //freezeTime = Mathf.Floor(Mathf.Log(hitMagnitude.magnitude, 1.25f)) *.01666f;
        freezeTime = fTime;
        freezePos = transform.position;
        paddle.StartFreeze(freezeTime);
        //Debug.Log(freezeTime.ToString());
        //if(fTime > 0f)
        VTCamera.OnHitImpact(cameraImpact, fTime);
            
        GameEngine.instance.IncreaseCombo();
        StatManager.AddEvent(new PlayerStats(desiredPossession, PlayerStats.Type.Hit, 1));
        if (hitMagnitude.magnitude > 20f)
        {
            StatManager.AddEvent(new PlayerStats(desiredPossession, PlayerStats.Type.Slam, hitMagnitude.magnitude));
        }

        hits++;
        if (hits >= 3)
        {
            minSpeed += speedIncrement;
            //GetComponent<Rigidbody2D>().velocity *= (maxSpeed / (maxSpeed - .02f));
        }

        ArduinoManager.OnPaddleHit(paddle.playerID);
        GridManager.instance.StartRipple(transform.position);
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Bounds")
        {
            if(GetComponent<Rigidbody2D>().velocity.magnitude >= maxSpeed * 1.25f)
            {
                Debug.Log("Winner!");
                StatManager.AddEvent(new PlayerStats(curPossession, PlayerStats.Type.Winner, GetComponent<Rigidbody2D>().velocity.magnitude));
            }
            GameEngine.instance.OnScore(this, curPossession);
            Instantiate(onScoreAnim[curPossession], transform.position, transform.rotation);
            GridManager.instance.StartColorRipple(transform.position, gridColors[curPossession]);
            //GridManager.instance.StartRipple(transform.position);
            RemoveFromGame();
        }
    }

    void RemoveFromGame()
    {
        transform.position = new Vector3(999f, 999f, 0f);
        var rb = GetComponent<Rigidbody2D>();
        rb.velocity = spinVec = Vector2.zero;
        spinVecV2 = 0f;
        rb.angularVelocity = 0f;
        freezeTime = 0f;
    }
    public int Reset()
    {
        curPossession = lastServe == 1 ? 0 : lastServe == 0 ? 1 : Random.Range(0, 2);
        onDesiredPossessionChange.Invoke(curPossession);
        lastServe = curPossession;
        desiredPossession = curPossession;
        SetBody();

        resetFrames = 3;
        hits = 0;
        minSpeed = initialMinSpeed;
        transform.position = Vector3.zero;
        spinVec = Vector2.zero;
        spinVecV2 = 0f;
        var vel = new Vector2(0f, (curPossession == 0 ? 5.5f : -5.5f) * GameEngine.invertFloat);
        var randAngle = Random.Range(15f, 25f) * (Random.Range(0, 2) == 1 ? 1f : -1f);
        vel = Quaternion.Euler(0f, 0f, randAngle) * vel;
        //if (curPossession == 0) vel = GameEngine.instance.p2Paddle.parent.spinTarget.transform.position;
        //else if (curPossession == 1) vel = GameEngine.instance.p1Paddle.parent.spinTarget.transform.position; // GameEngine.instance.p1Paddle.transform.position;
        //else vel = Random.insideUnitCircle;

        vel = vel.normalized * minSpeed * .5f;
        GetComponent<Rigidbody2D>().velocity = Vector2.zero;
        GetComponent<Rigidbody2D>().angularVelocity = 0f;
        TimeControl.StartTimer(1f, () => {
            GetComponent<Rigidbody2D>().velocity = vel;
            GameEngine.instance.HideServeArrow();
            }
        );
        GameEngine.instance.SetServeArrow(vel.normalized);
        return curPossession;
    }

    void SetBody()
    {
        neutralBody.SetActive(curPossession < 0);
        blueBody.SetActive(curPossession == 0);
        redBody.SetActive(curPossession == 1);

        if(curPossession >= 0)
            gridGlowLight.color = lightColors[curPossession];
    }
}
