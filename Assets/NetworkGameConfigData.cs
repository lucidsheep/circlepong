﻿using UnityEngine;

public class NetworkGameConfigData : ServerDataBase
{
    public float ball_speed;
    public float ball_drag;
    public float ball_spin;
    public float paddle_speed;
    public float paddle_charge_speed;
    public float paddle_power;
    public float paddle_charge_power;
    public float paddle_smash_multiplier;
}