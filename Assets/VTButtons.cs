﻿using UnityEngine;

public class VTButtons
{
    public static KeyCode P1Left = KeyCode.Q;
    public static KeyCode P1Right = KeyCode.W;
    public static KeyCode P2Left = KeyCode.S;
    public static KeyCode P2Right = KeyCode.A;
    public static KeyCode Credit = KeyCode.G;

    public struct ButtonState
    {
        public bool pressed;
        public bool down;
        public bool released;
    }
    public static ButtonState p1Left;
    public static ButtonState p1Right;
    public static ButtonState p2Left;
    public static ButtonState p2Right;

    public static int p1SpinDelta = 0;
    public static int p2SpinDelta = 0;
    public static void ButtonUpdate(bool isP2, bool isRight, bool button)
    {
        ButtonState state = isP2 ? (isRight ? p2Right : p2Left) : (isRight ? p1Left : p1Right);
        state.pressed = !state.down && button;
        state.released = state.down && !button;

        state.down = button;

        if (isP2)
        {
            if (isRight)
                p2Right = state;
            else
                p2Left = state;
        }
        else
        {
            if (isRight)
                p1Right = state;
            else
                p1Left = state;
        }
    }
    public static bool GetCreditInput()
    {
        return Input.GetKeyDown(Credit);
    }
    public static bool GetLeftButtonDown(int player)
    {
        return player == 0 ? p1Left.pressed : p2Left.pressed;

        if (player == 0) return Input.GetKeyDown(P1Left);
        return Input.GetKeyDown(P2Left);
    }

    public static bool GetRightButtonDown(int player)
    {
        return player == 0 ? p1Right.pressed : p2Right.pressed;

        if (player == 0) return Input.GetKeyDown(P1Right);
        return Input.GetKeyDown(P2Right);
    }
    public static bool GetAnyButtonDown(int player) { return GetLeftButtonDown(player) || GetRightButtonDown(player); }
    public static bool GetAnyButtonDown() { return GetAnyButtonDown(0) || GetAnyButtonDown(1); }

    public static bool GetLeftButton(int player)
    {
        return player == 0 ? p1Left.down : p2Left.down;
        if (player == 0) return Input.GetKey(P1Left);
        return Input.GetKey(P2Left);
    }

    public static bool GetRightButton(int player)
    {
        return player == 0 ? p1Right.down : p2Right.down;

        if (player == 0) return Input.GetKey(P1Right);
        return Input.GetKey(P2Right);
    }
    public static bool GetAnyButton(int player) { return GetLeftButton(player) || GetRightButton(player); }
    public static bool GetAnyButton() { return GetAnyButton(0) || GetAnyButton(1); }

    public static bool GetLeftButtonUp(int player)
    {
        return player == 0 ? p1Left.released : p2Left.released;

        if (player == 0) return Input.GetKeyUp(P1Left);
        return Input.GetKeyUp(P2Left);
    }

    public static bool GetRightButtonUp(int player)
    {
        return player == 0 ? p1Right.released : p2Right.released;


        if (player == 0) return Input.GetKeyUp(P1Right);
        return Input.GetKeyUp(P2Right);
    }
    public static bool GetAnyButtonUp(int player) { return GetLeftButtonUp(player) || GetRightButtonUp(player); }
    public static bool GetAnyButtonUp() { return GetAnyButtonUp(0) || GetAnyButtonUp(1); }

    static char btoc(bool b) { return b ? '1' : '0'; }
    public static string ButtonStateToString() { return "" + btoc(GetLeftButton(0)) + btoc(GetRightButton(0)) + btoc(GetLeftButton(1)) + btoc(GetRightButton(1)); }
}