﻿using UnityEngine;
using DG.Tweening;
public class VTCamera : MonoBehaviour
{
    public static Camera cam;
    public static VTCamera instance;
    public static float zoomLevel { get { return _zoomLevel;  } }
    static float _zoomLevel;
    private void Awake()
    {
        cam = Camera.main;
        instance = this;
    }

    private void Start()
    {
        GameEngine.instance.onModelChanged.AddListener(OnModel);
        OnModel();
    }
    void OnModel()
    {
        _zoomLevel = VTSaveManager.data.cameraZoom * -1f;
        Util.EditTransform(cam.gameObject, _zoomLevel, 4);
    }
    public static void OnHitImpact(Vector2 impact, float duration)
    {
        Vector3 v3Impact = new Vector3(impact.x, impact.y, zoomLevel);
        DOTween.Sequence()
                .Append(cam.transform.DOMove(v3Impact, duration))
                .Append(cam.transform.DOMove(new Vector3(0f, 0f, zoomLevel), duration));
    }
}