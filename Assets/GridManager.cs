﻿using System;
using UnityEngine;
using System.Collections.Generic;

public class GridManager : MonoBehaviour
{
    public VectorGrid grid;
    public Ball ball;
    public GameObject p1Paddle, p1Left, p1Right;
    public GameObject p2Paddle, p2Left, p2Right;

    public float force;
    public float force_radius;

    public float hit_force;
    public float hit_force_radius;

    public float suck_force;
    public float suck_force_radius;

    public float paddle_glow_radius;
    Vector3 ripple;
    float rippleDuration = 0f;
    Color rippleColor;
    float rippleThickness = .5f;
    List<GameObject> p1Lights;
    List<GameObject> p2Lights;

    public static GridManager instance;

    private void Awake()
    {
        instance = this;
        p1Lights = new List<GameObject> { p1Paddle, p1Left, p1Right };
        p2Lights = new List<GameObject> { p2Paddle, p2Left, p2Right };
    }

    private void Start()
    {
        GameEngine.instance.onPossessionChange.AddListener(OnPossessionChange);
    }

    void OnPossessionChange(int newOwner)
    {
        //StartColorRipple(ball.transform.position, ball.lightColors[newOwner]);
    }
    private void Update()
    {
        var possession = ball.possession;
        grid.AddGridForce(ball.transform.position, force, force_radius, ball.gridColors[Mathf.Clamp(ball.possession, 0, 1)], VectorGrid.ColorMode.Instant);
        foreach (var go in p1Lights)
        {
            grid.AddGridForce(go.transform.position, 0f, paddle_glow_radius, possession == 0 ? ball.lightColors[0] : ball.lightColors[0], VectorGrid.ColorMode.Instant);
        }
        foreach(var go in p2Lights)
        {
            grid.AddGridForce(go.transform.position, 0f, paddle_glow_radius, possession == 1 ? ball.lightColors[1] : ball.lightColors[1], VectorGrid.ColorMode.Instant);
        }

        if (rippleDuration > 0f)
        {
            rippleDuration -= Time.deltaTime;
            var radius = (((.75f - rippleDuration) / .75f )* 15f);
            grid.AddGridForce(ripple, 0f, radius, rippleColor, VectorGrid.ColorMode.Instant);
            //if (radius > rippleThickness)
            //    grid.AddGridForce(ripple, 0f, radius - rippleThickness, grid.m_ThinLineSpawnColor, VectorGrid.ColorMode.Instant);
        }
        
    }

    public void StartRipple(Vector3 position)
    {
        grid.AddGridForce(position, hit_force, hit_force_radius, ball.gridColors[Mathf.Clamp(ball.possession, 0, 1)], VectorGrid.ColorMode.Off);
    }

    public void StartColorRipple(Vector3 position, Color color)
    {
        ripple = position;
        rippleDuration = .5f;
        rippleColor = color;
    }

    public void StartSuck(Vector3 position, int playerID, float forceMultiplier = 1f)
    {
        grid.AddGridForce(position, suck_force, suck_force_radius * forceMultiplier, ball.gridColors[playerID], VectorGrid.ColorMode.Instant);
    }
    internal void SetQuality(int graphicsQuality)
    {
        int gridSize = 50;
        float gridScale = .25f;
        if(graphicsQuality == 0)
        {
            grid.enabled = false;
        } else
        {
            gridSize = graphicsQuality == 1 ? 25 : 40;
            gridScale = graphicsQuality == 1 ? .5f : .35f;
            grid.enabled = true;
            grid.m_GridWidth = grid.m_GridHeight = gridSize;
            grid.m_GridScale = gridScale;
        }

    }
}