﻿using UnityEngine;

public class CreditManager : MonoBehaviour
{
    public static CreditManager instance;

    public static bool isFreeplay { get { return VTSaveManager.data.coinsPerCredit == 0; } }
    public static float creditsDecimal { get { if (VTSaveManager.data.coinsPerCredit == 0) return 1f;  return (float)VTSaveManager.data.coinCount / (float)VTSaveManager.data.coinsPerCredit; } }
    public static int creditsInt { get { if (VTSaveManager.data.coinsPerCredit == 0) return 1; return VTSaveManager.data.coinCount / VTSaveManager.data.coinsPerCredit; } }
    public static int creditsIntRaw { get { return VTSaveManager.data.coinCount; } }

    public static float creditsToMoney(float credits) { return credits * .25f; } //todo - handle real currency
    public static string creditsString { get { 
            if (VTSaveManager.data.coinsPerCredit == 0) return "FREEPLAY";
            if (VTSaveManager.data.coinCount % VTSaveManager.data.coinsPerCredit == 0)
                return creditsInt.ToString(); //hide decimal on whole credits
            return Util.FractionToString(VTSaveManager.data.coinCount, VTSaveManager.data.coinsPerCredit, false, true); 
        } }
    
    public static bool readyToPlay { get { return creditsInt > 0; } }

    public CreditInsertedEvent onCreditInserted = new CreditInsertedEvent();
    public CreditInsertedEvent onCreditConsumed = new CreditInsertedEvent();

    public static bool StartGame()
    {
        if (!readyToPlay) return false;
        VTSaveManager.data.coinCount -= VTSaveManager.data.coinsPerCredit;
        VTSaveManager.instance.SaveData();
        instance.onCreditConsumed.Invoke(-1, "gamestart", false);
        return true;
    }
    private void Awake()
    {
        instance = this;
    }

    private void Update()
    {
        if(VTButtons.GetCreditInput())
        {
            var before = creditsInt;
            VTSaveManager.data.coinCount++;
            VTSaveManager.instance.SaveData();
            onCreditInserted.Invoke(1, "operator", before == 0 && creditsInt >= 1);
        }
    }
}