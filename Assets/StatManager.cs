﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System;
using System.Collections.Generic;
using System.Globalization;

public class StatManager : MonoBehaviour
{
    public class LogLine
    {
        public enum Type { CREDIT_IN, CREDIT_OUT, GAME_END, SERIES_END, POWER_ON}
        public string type;
        public DateTime time;
        public int version;
        public Dictionary<string, string> sVals = new Dictionary<string, string>();
        public Dictionary<string, float> fVals = new Dictionary<string, float>();
        public Dictionary<string, int> iVals = new Dictionary<string, int>(); //store bools as an int of 0 or 1
    }
    public static StatManager instance;
    public List<PlayerStats> allEvents = new List<PlayerStats>();
    public List<PlayerStats> pointEvents = new List<PlayerStats>();

    public static List<PlayerStats> allStats {  get { return instance.allEvents; } }

    float pointTime = 0f;
    float gameTime = 0f;
    int pointID = 0;

    static bool isLogging = false;
    public static int currentPointID { get { return instance.pointID; } }
    public static float totalGameTime { get { return instance.gameTime; } }
    
    public static string currentTimeStamp { get { return DateTime.Now.ToString("yyyy-mm-dd-HH-mm-ss"); } }

    bool gameStarted = false;
    bool pointStarted = false;
    void Awake()
    {
        instance = this;
    }

    public void OnGameStart()
    {
        gameStarted = true;
        gameTime = 0f;
        pointID = 0;
        allEvents = new List<PlayerStats>();
    }

    public void OnPointStart()
    {
        pointStarted = true;
        pointTime = 0f;
    }

    public int OnPointEnd(int winningPlayer)
    {
        int losingPlayer = 1 - winningPlayer;
        pointEvents.Add(new PlayerStats(winningPlayer, PlayerStats.Type.PointEnd, pointTime));
        pointEvents.Add(new PlayerStats(winningPlayer, PlayerStats.Type.RallyLength, pointEvents.FindAll(x => x.type == PlayerStats.Type.Hit).Count));
        var allHits = pointEvents.FindAll(x => x.type == PlayerStats.Type.Hit);
        allHits.Sort((x, y) => x.timeStamp.CompareTo(y.timeStamp));
        int lastHitID = allHits.Count == 0 ? winningPlayer : allHits[allHits.Count - 1].playerID;
        if(lastHitID != winningPlayer) //if loser hit ball last, it's a foul
        {
            pointEvents.Add(new PlayerStats(losingPlayer, PlayerStats.Type.Foul, 1));
        }
        foreach (PlayerStats e in pointEvents)
        {
            e.AddTag(winningPlayer == e.playerID ? PlayerStats.Tag.wonPoint : PlayerStats.Tag.lostPoint);
            allEvents.Add(e);
        }
        
        pointEvents = new List<PlayerStats>();
        var thisPoint = pointID;
        pointID++;
        pointStarted = false;
        return thisPoint;
    }

    public void OnGameEnd(int winningPlayer)
    {
        Debug.Log("game time " + Util.FormatTime(gameTime));

        
        var allRallies = allEvents.FindAll(x => x.type == PlayerStats.Type.RallyLength);
        int count = allRallies.Count;
        int totalRallies = Util.Fold(allRallies, (cur, total) => total += cur.intVal, 0);
        Debug.Log("avg rally " + (float)totalRallies / (float)count);
    }

    public static void AddEvent(PlayerStats ev)
    {
        if (GameEngine.state != GameEngine.State.Gameplay) return; //don't log events in demo mode
        ev.timeStamp = instance.gameTime;
        ev.pointID = instance.pointID;
        instance._AddEvent(ev);
    }
    void _AddEvent(PlayerStats ev)
    {
        pointEvents.Add(ev);
    }
    public static List<PlayerStats> GetPointData(int pid)
    {
        if (pid == instance.pointID)
            return instance.pointEvents;

        return instance.allEvents.FindAll(x => x.pointID == pid);
    }
    void Update()
    {
        if (pointStarted)
            pointTime += Time.deltaTime;
        if (gameStarted)
            gameTime += Time.deltaTime;
    }

    void LoadLogData()
    {

    }
    public static void StartLogging()
    {
        if (isLogging) return;
        isLogging = true;
        if (File.Exists(Application.persistentDataPath + "/log.txt"))
        {
            instance.LoadLogData();
            //StreamReader reader = new StreamReader(Application.persistentDataPath + "/masterlog.csv");
            //masterLog = reader.ReadToEnd();
            //reader.Close();
        }
        string startupLine = "POWER_ON,1," + currentTimeStamp + ",0"; //todo - count power ons in save data
        instance.WriteLogLine(startupLine);
        GameEngine.instance.onGameStateChange.AddListener(OnGameState);
        CreditManager.instance.onCreditInserted.AddListener((delta, source, _) => LogCreditInEvent(delta, source));
        CreditManager.instance.onCreditConsumed.AddListener((delta, source, _) => LogCreditOutEvent(delta, source));
        isLogging = true;
    }

    static void OnGameState(GameEngine.State before, GameEngine.State after)
    {
        if (after == GameEngine.State.Stats)
        {
            var allRallies = StatManager.allStats.FindAll(x => x.type == PlayerStats.Type.RallyLength);
            int count = allRallies.Count;
            int totalRallies = Util.Fold(allRallies, (cur, total) => total += cur.intVal, 0);
            allRallies.Sort((x, y) => x.intVal.CompareTo(y.intVal));
            int longestRally = allRallies[allRallies.Count - 1].intVal;
            float avgRally = Util.Round((float)totalRallies / (float)count, 2);
            int winnerID = GameEngine.instance.GetScore(0) > GameEngine.instance.GetScore(1) ? 0 : 1;
            string line = "GAME_END,1," + currentTimeStamp + ",";
            line += StatManager.currentPointID //gameID - todo - increment game id 
                + "," + (winnerID == 0 ? "blue" : "red") //winType
                + "," + "unranked" //type
                + "," + -1 //winnerID
                + "," + -1 //loserID
                + "," + count //points
                + "," + longestRally //LRally
                + "," + avgRally //ARally
                + "," + totalGameTime; //time
            instance.WriteLogLine(line);
            var serverStats = GameEngine.instance.gameData;
            serverStats.average_rally = avgRally;
            serverStats.game_time = totalGameTime;
            serverStats.longest_rally = longestRally;
            serverStats.game_players = new GameStatData[2];
            for (int i = 0; i < 2; i++)
            {
                var points = StatManager.allStats.FindAll(x => x.playerID == i && x.type == PlayerStats.Type.PointEnd).Count;
                var hits = StatManager.allStats.FindAll(x => x.playerID == i && x.type == PlayerStats.Type.Hit).Count;
                var allSlams = StatManager.allStats.FindAll(x => x.playerID == i && x.type == PlayerStats.Type.Slam);
                var slams = allSlams.Count;
                var fouls = StatManager.allStats.FindAll(x => x.playerID == i && x.type == PlayerStats.Type.Foul).Count;
                var winners = StatManager.allStats.FindAll(x => x.playerID == i && x.type == PlayerStats.Type.Winner).Count;
                var slices = StatManager.allStats.FindAll(x => x.playerID == i && x.type == PlayerStats.Type.Slice).Count;
                var trickShots = StatManager.allStats.FindAll(x => x.playerID == i && x.type == PlayerStats.Type.Trick).Count;
                var smashShots = StatManager.allStats.FindAll(x => x.playerID == i && x.type == PlayerStats.Type.Smash).Count;

                var thisStats = new GameStatData();
                if (i == 0)
                    thisStats.player_id = GameEngine.instance.p1PlayerData == null ? 0 : GameEngine.instance.p1PlayerData.player_id;
                else
                    thisStats.player_id = GameEngine.instance.p2PlayerData == null ? 0 : GameEngine.instance.p2PlayerData.player_id;
                thisStats.side_id = i;
                thisStats.result_type = winnerID == i ? 1 : 0;
                thisStats.points = GameEngine.instance.GetScore(i);
                thisStats.shots_ace = winners;
                thisStats.shots_fault = fouls;
                thisStats.shots_power = slams;
                thisStats.shots_slice = trickShots;
                thisStats.shots_smash = smashShots;
                serverStats.game_players[i] = thisStats;
            }
            GameEngine.instance.gameData = serverStats;
            WebManager.OnGameEnd();
        }
    }

    public static void LogCreditOutEvent(int creditsConsumed, string type = "operator")
    {
        string ev = "CREDIT_OUT,1," + currentTimeStamp +"," + Mathf.Abs(creditsConsumed) + "," + CreditManager.creditsIntRaw + "," + type;
        instance.WriteLogLine(ev);
    }
    public static void LogCreditInEvent(int creditsAdded, string type = "operator")
    {
        string ev = "CREDIT_IN,1," + currentTimeStamp + "," + creditsAdded + "," + CreditManager.creditsIntRaw + "," + type + "," + CreditManager.creditsToMoney(creditsAdded);
        instance.WriteLogLine(ev);
    }
    public void WriteLogLine(string line)
    {
        if (!isLogging) return;
        StreamWriter writer = new StreamWriter(Application.persistentDataPath + "/log.txt", true);
        writer.WriteLine(line);
        writer.Close();
    }
    public static LogLine fromString(string data)
    {
        LogLine ret = new LogLine();
        string[] vals = data.Split(',');
        ret.type = vals[0];
        ret.version = int.Parse(vals[1]);
        ret.time = DateTime.Parse(vals[2]);
        switch(ret.type)
        {
            case "CREDIT_IN":
                ret.iVals.Add("added", int.Parse(vals[3]));
                ret.iVals.Add("total", int.Parse(vals[4]));
                ret.sVals.Add("type", vals[5]);
                ret.fVals.Add("money", float.Parse(vals[6]));
                break;
            case "CREDIT_OUT":
                ret.iVals.Add("used", int.Parse(vals[3]));
                ret.iVals.Add("total", int.Parse(vals[4]));
                ret.sVals.Add("type", vals[5]);
                break;
        }
        return ret;
    }

}
