﻿[System.Serializable]
public class PlayerData : ServerDataBase
{
    [System.Serializable]
    public struct colorPrefs
    {
        public int side_id;
        public string main_color;
        public string accent_color;
    }
    public int player_id;
    public int side_id;
    public string password;
    public string secret_key;
    public colorPrefs color;
}