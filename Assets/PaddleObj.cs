﻿using UnityEngine;
using System.Collections;

public class PaddleObj : MonoBehaviour
{

    public Paddle parent;
    public SpriteRenderer sprite;
    public Material shader;

    private void Start()
    {
        shader = sprite.material;
    }
    public float spin { get { return parent.spin; } }
    public Vector2 spinVec {  get { return parent.spinVec; } }
}
