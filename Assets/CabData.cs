﻿[System.Serializable]
public class CabData : ServerDataBase
{
    [System.Serializable]
    public struct colorPrefs
    {
        public int side_id;
        public string main_color;
        public string accent_color;
    }
    public int cab_id;
    public string password;
    public string secret_key;
    public colorPrefs color;
}