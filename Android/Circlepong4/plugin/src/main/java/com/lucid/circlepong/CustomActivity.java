package com.lucid.circlepong;
import com.hoho.android.usbserial.driver.UsbSerialDriver;
import com.hoho.android.usbserial.driver.UsbSerialPort;
import com.hoho.android.usbserial.driver.UsbSerialProber;
import com.hoho.android.usbserial.util.SerialInputOutputManager;
import com.unity3d.player.UnityPlayer;
import com.unity3d.player.UnityPlayerActivity;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.hardware.usb.UsbDeviceConnection;
import android.hardware.usb.UsbManager;
import android.os.Bundle;

import android.os.Handler;
import android.os.Looper;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.InputDevice;
import android.view.View;
import android.view.Display;
import android.view.Window;
import android.view.WindowManager;

import java.io.IOException;
import java.util.List;

import static java.lang.System.in;

public class CustomActivity extends Activity implements SerialInputOutputManager.Listener {

    protected UnityPlayer mUnityPlayer;

    protected static CustomActivity instance;
    protected static float accumulatedX;
    protected static float accumulatedY;
    protected UsbSerialPort port;
    protected SerialInputOutputManager usbIOManager;
    boolean portOpen = false;
    byte[] buffer;
    protected static int buttonStates = 0;
    protected static Handler handler;
    public CustomActivity()
    {
        instance = this;
        accumulatedY = accumulatedX = 0;
    }

    public static float GetXDelta()
    {
        float ret = accumulatedX;
        accumulatedX = 0;
        return ret;
    }

    public static float GetYDelta()
    {
        float ret = accumulatedY;
        accumulatedY = 0;
        return ret;
    }

    public static int GetButtonStates()
    {
        return buttonStates;
    }
    public static void SetDisplayMode(int displayMode)
    {
        WindowManager.LayoutParams params = instance.getWindow().getAttributes();
        params.preferredDisplayModeId = displayMode;
        instance.getWindow().setAttributes(params);
    }

    public static String GetDisplayModeOptions()
    {
        String ret = "";
        final Display.Mode[] modes = instance.getWindowManager().getDefaultDisplay().getSupportedModes();
        for(int i = 0; i < modes.length; i++)
        {
            ret += modes[i].getModeId() + ":" + modes[i].getPhysicalWidth() + "x" + modes[i].getPhysicalHeight() + " " + modes[i].getRefreshRate() + "\n";
        }
        return ret;
    }
    public static void StartAndroidApp(String packageName)
    {
        Intent launchIntent = instance.getPackageManager().getLaunchIntentForPackage(packageName);
        instance.startActivity( launchIntent );
    }
    public static void ArduinoMessage(byte[] bytesToSend)
    {
        if(!instance.portOpen) return;
        try {
            //instance.usbIOManager.writeAsync(bytesToSend);
            instance.port.write(bytesToSend, 1000);
        } catch (IOException e)
        {
            instance.mUnityPlayer.UnitySendMessage("Singletons", "OnAndroidCreate", "Write.IOException: " + e.getMessage());
        }
    }

    public static void GetArduinoUpdate()
    {
        if(!instance.portOpen) return;

        try {
            for(int i = 0; i < 64; i++)
                instance.buffer[i] = 0;
            instance.port.read(instance.buffer, 1000);
            for(int i = 0; i < 64; i++)
            {
                boolean endOfMessage = instance.buffer[i] == 0;
                if(endOfMessage) break;
                byte thisByte = instance.buffer[i];
                boolean p1 = (thisByte & 64) == 0;
                if((thisByte & 128) > 0) //spinner
                {
                    boolean isNegative = (thisByte & 32) > 0;
                    int value = (thisByte & 31) * (isNegative ? -1 : 1);
                    if(p1) accumulatedX += value;
                    else accumulatedY += value;
                } else //button
                {
                    if(p1) buttonStates = buttonStates & 0b11111100;
                    else    buttonStates = buttonStates & 0b11110011;
                    if((thisByte & 1) > 0) buttonStates += p1 ? 1 : 4;
                    if((thisByte & 8) > 0) buttonStates += p1 ? 2 : 8;
                }

            }
        } catch(IOException e)
        {
            instance.mUnityPlayer.UnitySendMessage("Singletons", "OnAndroidCreate", "Read.IOException: " + e.getMessage());
            //instance.portOpen = false;
        }


    }
    protected String updateUnityCommandLineArguments(String var1) {
        return var1;
    }

    @Override public void onWindowFocusChanged(boolean hasFocus)
    {
        super.onWindowFocusChanged(hasFocus);
        this.mUnityPlayer.windowFocusChanged(hasFocus);
        if(hasFocus)
        {
            mUnityPlayer.getChildAt(0).requestPointerCapture();
            //mUnityPlayer.requestPointerCapture();
        }



    }
    @Override protected void onCreate(Bundle savedInstanceState) {
        this.requestWindowFeature(1);
        super.onCreate(savedInstanceState);
        String var2 = this.updateUnityCommandLineArguments(this.getIntent().getStringExtra("unity"));
        this.getIntent().putExtra("unity", var2);
        this.mUnityPlayer = new UnityPlayer(this);
        this.setContentView(this.mUnityPlayer);
        this.mUnityPlayer.requestFocus();

        mUnityPlayer.UnitySendMessage("Singletons", "OnAndroidCreate", "Hello from Android!");
        mUnityPlayer.getChildAt(0).setOnCapturedPointerListener(new View.OnCapturedPointerListener() {
            @Override
            public boolean onCapturedPointer (View view, MotionEvent motionEvent) {
                //motionEvent.getRawX()
                //motionEvent.getAxisValue(MotionEvent.AXIS_RELATIVE_X);
                accumulatedY += motionEvent.getY() * motionEvent.getYPrecision();
                accumulatedX += motionEvent.getX() * motionEvent.getXPrecision();
                /*
                for (int i = 0; i < motionEvent.getHistorySize(); i++) {
                    accumulatedX += motionEvent.getHistoricalAxisValue(MotionEvent.AXIS_X, i);
                }
                */

                //accumulatedY += motionEvent.getAxisValue(MotionEvent.AXIS_RELATIVE_Y);
                return true;
            }
            });

        UsbManager manager = (UsbManager) getSystemService(Context.USB_SERVICE);
        List<UsbSerialDriver> availableDrivers = UsbSerialProber.getDefaultProber().findAllDrivers(manager);
        for (UsbSerialDriver d:availableDrivers
             ) {
            mUnityPlayer.UnitySendMessage("Singletons", "OnAndroidCreate", "USB vendor ID: " + d.getDevice().getVendorId() );
            if(d.getDevice().getVendorId() == 9025) //arduino id
            {
                //open usb connection
                UsbDeviceConnection connection = manager.openDevice(d.getDevice());
                if(connection == null)
                {
                    mUnityPlayer.UnitySendMessage("Singletons", "OnAndroidCreate", "Requesting permission...");
                    final Boolean[] granted = {null};
                    BroadcastReceiver usbReceiver = new BroadcastReceiver() {
                        @Override
                        public void onReceive(Context context, Intent intent) {
                            granted[0] = intent.getBooleanExtra(UsbManager.EXTRA_PERMISSION_GRANTED, false);
                        }
                    };
                    PendingIntent permissionIntent = PendingIntent.getBroadcast(getApplicationContext(), 0, new Intent("com.lucid.circleponggame"), 0);
                    IntentFilter filter = new IntentFilter("com.lucid.circleponggame");
                    getApplicationContext().registerReceiver(usbReceiver, filter);
                    manager.requestPermission(d.getDevice(), permissionIntent);
                    for(int i=0; i<5000; i++) {
                        if(granted[0] != null) break;
                        try {
                            Thread.sleep(1);
                        } catch (Exception e) { return;}
                    }
                    mUnityPlayer.UnitySendMessage("Singletons", "OnAndroidCreate", "Permission status: " + granted[0]==null?"false":granted[0].toString());
                    if(granted[0]==null || !granted[0])
                        return;
                    connection = manager.openDevice(d.getDevice());
                    if(connection == null)
                    {
                        mUnityPlayer.UnitySendMessage("Singletons", "OnAndroidCreate", "Permission double denied!");
                        return;
                    }
                }
                port = d.getPorts().get(0); // Most devices have just one port (port 0)
                try {
                    mUnityPlayer.UnitySendMessage("Singletons", "OnAndroidCreate", "Opening port");
                    port.open(connection);
                    mUnityPlayer.UnitySendMessage("Singletons", "OnAndroidCreate", "Port opened");
                    port.setParameters(19200, 8, UsbSerialPort.STOPBITS_1, UsbSerialPort.PARITY_NONE);
                    //usbIOManager = new SerialInputOutputManager(port, this);
                    //usbIOManager.run();
                    mUnityPlayer.UnitySendMessage("Singletons", "OnAndroidCreate", "Parameters set");
                    portOpen = true;
                    buffer = new byte[64];
                    //start serial loop
                    handler = new Handler(Looper.getMainLooper());
                    handler.postDelayed(new Runnable() {
                            public void run() {
                                GetArduinoUpdate();
                                handler.postDelayed(this, 20);
                            }
                        }, 20);

                } catch(IOException e)
                {
                    mUnityPlayer.UnitySendMessage("Singletons", "OnAndroidCreate", "Init.IOException: " + e.getMessage());
                }
            }
        }
    }

    //copy paste from UnityPlayerActivity
    protected void onNewIntent(Intent var1) {
        this.setIntent(var1);
        this.mUnityPlayer.newIntent(var1);
    }

    protected void onDestroy() {
        this.mUnityPlayer.destroy();
        super.onDestroy();
    }

    protected void onPause() {
        super.onPause();
        this.mUnityPlayer.pause();
    }

    protected void onResume() {
        super.onResume();
        this.mUnityPlayer.resume();
    }

    protected void onStart() {
        super.onStart();
        this.mUnityPlayer.start();
    }

    protected void onStop() {
        super.onStop();
        this.mUnityPlayer.stop();
    }

    public void onLowMemory() {
        super.onLowMemory();
        this.mUnityPlayer.lowMemory();
    }

    public void onTrimMemory(int var1) {
        super.onTrimMemory(var1);
        if (var1 == 15) {
            this.mUnityPlayer.lowMemory();
        }

    }

    public void onConfigurationChanged(Configuration var1) {
        super.onConfigurationChanged(var1);
        this.mUnityPlayer.configurationChanged(var1);
    }

    public boolean dispatchKeyEvent(KeyEvent var1) {
        return var1.getAction() == 2 ? this.mUnityPlayer.injectEvent(var1) : super.dispatchKeyEvent(var1);
    }

    public boolean onKeyUp(int var1, KeyEvent var2) {
        return this.mUnityPlayer.injectEvent(var2);
    }

    public boolean onKeyDown(int var1, KeyEvent var2) {
        return this.mUnityPlayer.injectEvent(var2);
    }

    public boolean onTouchEvent(MotionEvent var1) {
        return this.mUnityPlayer.injectEvent(var1);
    }

    public boolean onGenericMotionEvent(MotionEvent var1) {
        /*
        if(var1.getSource() == InputDevice.SOURCE_MOUSE_RELATIVE)
        {
            mUnityPlayer.UnitySendMessage("Singletons", "OnAndroidCreate", "Relative!");
            java.lang.String ret = String.format("%s,%s", String.valueOf(var1.getX()), String.valueOf((var1.getY())));
            mUnityPlayer.UnitySendMessage("Singletons", "OnAndroidMouseMoved", ret);
        }
        */
        return this.mUnityPlayer.injectEvent(var1);
    }

    @Override
    public void onNewData(byte[] data) {
        for(int i = 0; i < data.length; i++)
        {
            boolean endOfMessage = data[i] == 0;
            if(endOfMessage) break;
            byte thisByte = data[i];
            boolean p1 = (thisByte & 64) == 0;
            if((thisByte & 128) > 0) //spinner
            {
                boolean isNegative = (thisByte & 32) > 0;
                int value = (thisByte & 31) * (isNegative ? -1 : 1);
                if(p1) accumulatedX += value;
                else accumulatedY += value;
            } else //button
            {
                if(p1) buttonStates = buttonStates & 0b11111100;
                else    buttonStates = buttonStates & 0b11110011;
                if((thisByte & 1) > 0) buttonStates += p1 ? 1 : 4;
                if((thisByte & 8) > 0) buttonStates += p1 ? 2 : 8;
            }

        }
    }

    @Override
    public void onRunError(Exception e) {
        mUnityPlayer.UnitySendMessage("Singletons", "OnAndroidCreate", "IOManager error:" + e.getMessage());

    }
}