package com.lucid.circlepong;

import android.content.Context;
import android.view.MotionEvent;

import com.unity3d.player.UnityPlayer;

public class CustomUnityPlayer extends UnityPlayer {
    public CustomUnityPlayer(Context var1) {
        super(var1);
    }
    @Override
    public boolean onCapturedPointerEvent(MotionEvent motionEvent) {
        java.lang.String ret = String.format("%s,%s", String.valueOf(motionEvent.getX()), String.valueOf((motionEvent.getY())));
        UnitySendMessage("Singletons", "OnAndroidMouseMoved", ret);
        return true;
    }

    @Override public void onPointerCaptureChange (boolean hasCapture)
    {
        super.onPointerCaptureChange(hasCapture);
        java.lang.String ret = String.format("CaptureChange %s", String.valueOf(hasCapture));
        UnitySendMessage("Singletons", "OnAndroidCreate", ret);

    }
}
